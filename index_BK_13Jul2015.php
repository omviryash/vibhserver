<?php 
include "includes/config.php";

//CLIENT INFO
$clients = $db->get_results("select * from client_info");
$client_ary = array();

foreach($clients as $c){
	$client_ary[$c->client_code] = $c;
}

$json_clients = json_encode($client_ary);

//ITEM INFO
$items = $db->get_results("select * from item_info");
$item_ary = array();

foreach($items as $i){
	$item_ary[$i->item_code] = $i;
}

$json_items = json_encode($item_ary);

//ACTION DATE
$actiondate = date('d/m/Y');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<style>
.tbl {
	font-family: Verdana, Geneva, sans-serif;
	font-size: 13px;
	border-collapse: collapse;
}
.tbl th {
	text-align: left;
}
tr.minus td{
	color:red;
}
.redtd{
	background-color:#ff;
}
.greentd{
	background-color:#090;
}
</style>
<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
<script>
var items = <?php echo $json_items ?>;
var clients = <?php echo $json_clients ?>;

function soda(actiontype, lot, qty, rate, itemcode, itemname, clientcode, clientname, actiontime, actiondate, actionserial, issaved){
	this.actiontype = actiontype;
	this.lot = lot;
	this.qty = qty;
	this.rate = rate;
	this.itemcode = itemcode;
	this.itemname = itemname;
	this.clientcode = clientcode;
	this.clientname = clientname;
	this.actiontime = actiontime;
	this.actiondate = actiondate;
	this.actionserial = actionserial;
	this.issaved = issaved;
}

var sodas = [];
//var soda1 = new soda('+',10,100,123,'1234','GLD','345','SUS','06/26/2015','03:15',2,false);

$(document).ready(function(e) {
	$("#lot").mask("9?999");
	$("#itemcode").mask("9?999");
	$("#actiondate").mask("99/99/9999");
	
    $('#actiontype').keypress(function(event){
		/*if(event.which == 13){
			if($('#actiontype').val() != ''){
				$('#lot').focus();
			}
		}*/
		if(event.which == 43 || event.which == 45){
			$('#lot').focus();
		}
		else if(event.which != 43 && event.which != 45){
			event.preventDefault();
		}
	});
	
	$('#lot').keypress(function(event){
		if(event.which == 13){
				$('#itemcode').focus();
		}
	});
	
	$('#rate').keypress(function(event){
		if(event.which == 13){
				$('#clientcode').focus();
		}
	});
	
	$('#itemcode').keypress(function(event){
		if(event.which == 13){
			$('#itemname').val(items[$('#itemcode').val()].item_name);
			$('#qty').val(items[$('#itemcode').val()].lot_qty * $('#lot').val());
			$('#rate').focus();
		}
	});
	
	$('#clientcode').keypress(function(event){
		if(event.which == 13){
			var currentdate = new Date(); 
			var actiontime = (currentdate.getHours()<10?('0' + currentdate.getHours()):currentdate.getHours()) + ":" + (currentdate.getMinutes()<10?('0' + currentdate.getMinutes()):currentdate.getMinutes());
			
			$('#clientname').val(clients[$('#clientcode').val()].client_name);

			var actiontype = $('#actiontype').val();
			var lot = $('#lot').val();
			var qty = $('#qty').val();
			var rate = $('#rate').val();
			var itemcode = $('#itemcode').val();
			var itemname = $('#itemname').val();
			var clientcode = $('#clientcode').val();
			var clientname = $('#clientname').val();
			var actiondate = $('#actiondate').val();
			var actionserial = $('#actionserial').val();
			
			var newentry = new soda(actiontype,lot,qty,rate,itemcode,itemname,clientcode,clientname,actiontime,actiondate,actionserial,0);
			sodas.push(newentry);
			
			$('#actionserial').val(parseInt(actionserial) + 1);

			var cls = '';
			if(actiontype == '-'){
				cls = 'minus';
			}
			
			var content = '<tr class="' + cls + '"><td>' + actiontype + '</td><td><div style="float:left; display:inline">' + lot + '</div><div style="float:right; display:inline">' + qty + '</div></td><td>' + rate + '</td><td><div style="float:left; display:inline">' + itemname + '</div><div style="float:right; display:inline">' + itemcode + '</div></td><td><div style="float:left; display:inline">' + clientcode + '</div><div style="float:right; display:inline">' + clientname + '</div></td><td>' + actiontime + '</td><td>' + actionserial + '</td><td class="redtd" id="actionserial' + actionserial + '"></td></tr>';
			
			$('#tbody').append(content);
			
			$('#actiontype').val('');
			$('#lot').val('');	
			$('#qty').val('');		
			$('#rate').val('');
			$('#itemcode').val('');
			$('#itemname').val('');
			$('#clientcode').val('');
			$('#clientname').val('');
			//$('#actiondate').val('');
			
			$('#actiontype').focus();
		}
	});
});

function savedata(){
	var unsaved_sodas = [];
	
	for(var i=0; i<sodas.length; i++){
		if(sodas[i].issaved == 0){
			unsaved_sodas.push(sodas[i]);
		}
	}
	
	$.post('aj-savedata.php',{ary:JSON.stringify(unsaved_sodas)},function(data, success){
			for(i=0; i<data.length; i++){
				$('#actionserial' + data[i]).removeClass('redtd');
				$('#actionserial' + data[i]).addClass('greentd');
				
				for(j=0; i<sodas.length; j++){
					if(sodas[j].actionserial == data[i]){
						sodas[j].issaved = 1;
					}
				}
			}
	},'json');
	

}

setInterval(function(){ savedata(); }, 5000);
</script>

</head>

<body>
<table border="0" cellpadding="5" cellspacing="5">
<tr>
<td>
Date: <input type="text" id="actiondate" value="<?php echo $actiondate ?>" />
</td>
</tr>
<tr>
<td>
<table width="950px" border="1" cellpadding="2" cellpadding="2" class="tbl">
<thead>
<tr>
  <th>+/-</th>
  <th><div style="float:left; display:inline">Lot</div>
    <div style="float:right; display:inline">Qty</div></th>
  <th>Rate</th>
  <th>Script</th>
  <th>Party</th>
  <th>Time</th>
  <th>Serial</th>
  <th>Status</th>
</tr>
</thead>
<tbody id="tbody">
<?php 
	$prev_data = mysql_query("select * from soda_info where actiondate='".$actiondate."' order by serial_no asc");
	$actionserial = "0";
	
	if(mysql_num_rows($prev_data)){
		while($row = mysql_fetch_array($prev_data)){
		?>	<tr class="<?php echo ($row["entry_type"]=='-'?'minus':'')?>"><td><?php echo $row["entry_type"];?></td>
  <td><div style="float:left; display:inline"><?php echo $row["lot"];?></div>
    <div style="float:right; display:inline"><?php echo $row["qty"];?></div></td>
  <td><?php echo $row["rate"];?></td>
  <td><div style="float:left; display:inline"><?php echo $row["item_name"];?></div><div style="float:right; display:inline"><?php echo $row["item_code"];?></div></td>
  <td><div style="float:left; display:inline"><?php echo $row["client_code"];?></div><div style="float:right; display:inline"><?php echo $row["client_name"];?></div></td>
  <td><?php echo $row["actiontime"];?></td>
  <td><?php echo $row["serial_no"];?></td>
  <td class="greentd"></td></tr>
  <?php $actionserial=$row["serial_no"];?>
		<?php }
	}
?>
</tbody>
<tfoot>
<tr>
  <td><input id="actiontype" style="width:30px; text-align:center;" maxlength="1" placeholder="+/-" /></td>
  <td><div style="float:left; display:inline">
      <input id="lot" style="width:70px" placeholder="Lot" />
    </div>
    <div style="float:right; display:inline">
      <input id="qty" style="width:70px" placeholder="Qty" />
    </div></td>
  <td><input id="rate" style="width:100px" placeholder="Rate" /></td>
  <td><div style="float:left; display:inline">
      <input id="itemname" style="width:150px" placeholder="Script Name" />
    </div>
    <div style="float:left; display:inline">
      <input id="itemcode" style="width:50px; margin-left:10px;" placeholder="Script Code" />
    </div>
</td>
  <td><div style="float:left; display:inline">
      <input id="clientcode" style="width:70px" placeholder="Party Code" />
    </div>
    <div style="float:left; display:inline">
      <input id="clientname" style="width:150px; margin-left:10px;" placeholder="Party Name" />
    </div></td>
  <td></td>
  <td><input type="hidden" id="actionserial" value="<?php echo $actionserial+1;?>" /></td>
  <td></td>
</tr>
</tfoot>
</table>
</td>
</tr>
</table>
</body>
</html>