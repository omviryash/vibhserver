<?php 
/* Database Connection Start*/ 
date_default_timezone_set("Asia/Kolkata");
session_start();
include_once "ez_sql_core.php"; 
include_once "ez_sql_mysql.php";

//=========== DATABASE =========== 
$db_user='root';		//DB User
$db_password='';	//DB Password 
$db_name='vibhserver'; 		//DB Name
$db_host='localhost';

$db =  new ezSQL_mysql($db_user,$db_password,$db_name,$db_host); 

/* Database Connection Ends */
$db->query( "SET NAMES 'utf8'" );
$db->query( "SET SESSION collation_connection ='utf8_unicode_ci'" ) ;

$conn = mysqli_connect($db_host,$db_user,$db_password,$db_name);
if(!$conn)
{
	echo mysqli_connect_error();
	die;
}


?>