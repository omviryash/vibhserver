<?php
session_start(); if(isset($_SESSION["logged_user_name"])) {
if(isset($_POST['submit'])) {
    $current_date1 	= $_POST['currentDate']."/".$_POST['currentMonth']."/".$_POST['currentYear'];
} else {
    $current_date1 = date('d/m/Y');
}
//echo $current_date1; die;
include_once('include/config.php');
	
## Delete Multiple Record 
if(isset($_POST["delete"])){
  //echo "<pre>";  print_r($_POST);
  if(!empty($_POST["ClientInfo"])){
    
    $GetClientInfoCSV =  implode(",", $_POST["ClientInfo"]);
    //echo $GetClientInfoCSV; 
    $sSQL = "DELETE FROM `soda_info` WHERE `id` IN (".$GetClientInfoCSV.")";
    $rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
    $_SESSION['success']="Record is deleted successfully.";
  }
}
$get_brok='0';
if(isset($_GET["brok"]) && $_GET["brok"]=='1')
{
	$get_brok='1';
}
$get_profitloss='0';
if(isset($_GET["profitloss"]) && $_GET["profitloss"]=='1')
{
	$get_profitloss='1';
}
$get_delete='0';
if(isset($_GET["delete"]) && $_GET["delete"]=='1')
{
	$get_delete='1';
}
$get_confirm='0';
if(isset($_GET["confirm"]) && $_GET["confirm"]=='1')
{
	$get_confirm='1';
}
$get_summary='0';
if(isset($_GET["summary"]) && $_GET["summary"]=='1')
{
	$get_summary='1';
}

$disp_soda_list = 1;
if ($get_brok == 1 || $get_profitloss == 1 || $get_summary == 1) {
	$disp_soda_list = 0;
}

function Percentage_ItemWise($clientCode,$brok1,$brok2, $calculate) {
	$brokerage = 0;
	$overNightBrokerage = 0;
	$intraDayAmount = 0;
	$intraDayBrokerage = 0;
	$overnightPercentage = ($brok2 != 0) ? $brok2 : $brok1;
	
	if(is_array($calculate)) {

		$overnightCount = abs($calculate['buy'] - $calculate['sell']);
		$intraDayCount = $calculate['amount'] - $overnightCount;
		$overnightPercentage = $overnightPercentage / 100;
		$intraDayPercentage = $brok1 / 100;
		

		$intraDayAmount = (($calculate['totalTransactionRate'] / $calculate['totalTransaction']) * 100) * $intraDayCount;
		$intraDayBrokerage = ($intraDayAmount * $intraDayPercentage) / 2 ;
		if($brok2 != 0){
			$overNightBrokerage = round(abs(($calculate['totalTransactionAmount'] - $intraDayAmount) * $overnightPercentage) / 2,2);
		}else{
			$overNightBrokerage += round(abs(($calculate['totalTransactionAmount'] - $intraDayAmount) * $overnightPercentage) / 2,2);
		}
	}
	return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
}

function Fixed_ItemWise($clientCode,$brok1,$brok2, $calculate) {
	$brokerage = 0;
	$overNightBrokerage = 0;
	$intraDayAmount = 0;
	$intraDayBrokerage = 0;
	$overnightPercentage = ($brok2 != 0) ? $brok2 : $brok1;
	if(is_array($calculate)) {

		$overnightCount = abs($calculate['buyAmount'] - $calculate['sellAmount']);
		$intraDayCount = $calculate['amount'] - $overnightCount;

		$intraDayBrokerage = ($intraDayCount * $brok1) / 2 ;
		if($brok2 != 0){
			$overNightBrokerage = round(abs( $overnightCount * $overnightPercentage),2);
		}else{
			$overNightBrokerage += round(abs( $overnightCount * $overnightPercentage),2);
		}
		
	}
	return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
}
/** 
 * function Percentage
 * Accept: client_code, calculation data
 * Return: Brokerage
 */
function Percentage($clientCode, $calculate) {
	$brokerage = 0;
	$overNightBrokerage = 0;
	$intraDayAmount = 0;
	$intraDayBrokerage = 0;
	foreach($calculate as $key => $data) {
		$overnightPercentage = ($calculate['brok2'] != 0) ? $calculate['brok2'] : $calculate['brok1'];
		$overnightPercentage = $overnightPercentage / 100;
		$intraDayPercentage = $calculate['brok1'] / 100;
		if(is_array($data)) {
			$overnightCount = abs($data['buy'] - $data['sell']);
			$intraDayCount = $data['amount'] - $overnightCount;
			$intraDayAmount = (($data['totalTransactionRate'] / $data['totalTransaction']) * 100) * $intraDayCount;
			$intraDayBrokerage += ($intraDayAmount * $intraDayPercentage) / 2 ;
			$overNightBrokerage += round(abs(($data['totalTransactionAmount'] - $intraDayAmount) * $overnightPercentage) / 2,2);
		}
	}
	return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
}

/** 
 * function Fixed
 * Accept: client_code, calculation data
 * Return: Brokerage
 */
function Fixed($clientCode, $calculate) {
	$brokerage = 0;
	$overNightBrokerage = 0;
	$intraDayAmount = 0;
	$intraDayBrokerage = 0;
	foreach($calculate as $key => $data) {
		$overnightPercentage = ($calculate['brok2'] != 0) ? $calculate['brok2'] : $calculate['brok1'];
		if(is_array($data)) {

			$overnightCount = abs($data['buyAmount'] - $data['sellAmount']);
			$intraDayCount = $data['amount'] - $overnightCount;

			$intraDayBrokerage += ($intraDayCount * $calculate['brok1']) / 2 ;
			$overNightBrokerage += round(abs( $overnightCount * $overnightPercentage) / 2,2);
		}
	}
	return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
}

function caclulate_brokerage($calculates) {
	$brokerage = array();
	foreach($calculates as $key => $calculate) {
		/** 
		 * Call function named Fixed or Percentage
		 * Accept: client_code, calculation data
		 * Return: Brokerage
		 */
		$brokerage[$key] = $calculate['brok_type']($key,$calculate);
	}
	return $brokerage;
}

function getOpening($client_code, $actiondate, $item_code) {
	global $conn;
	$getOpeningQ = "SELECT * FROM `soda_info` WHERE `soda_status` = '-1' AND `item_code` LIKE '".$item_code . "' AND `client_code` LIKE '".$client_code . "' AND `actiondate` LIKE '" . $actiondate ."'";
	$getOpeningR = mysqli_query($conn, $getOpeningQ) or die(mysqli_error());
	while($openingRow = mysqli_fetch_assoc($getOpeningR)) {
		?>
		<tr <?php if($openingRow['entry_type']=="+"){ echo "style=color:#0000FF;"; } else { echo "style=color:#FF0000;"; } ;?>>
			<?php if($get_delete=='1'){ ?><td><input type="checkbox" name="ClientInfo[]" value="<?php echo $openingRow['id']; ?>" /></td><?php } ?>
            <?php if($get_confirm=='1'){ ?><td><input type="text" class="cls_confirm" id="<?php echo $openingRow['id']; ?>" maxlength="1" value="<?php echo $openingRow['confirm'];  ?>" /></td><?php } ?>
            <td><?php echo $openingRow['entry_type'];?></td>
            <td style="text-align:right;"><?php echo $openingRow['lot']; ?> (<?php echo $openingRow['qty'];?>)</td>
            <td style="text-align:right;"><?php echo $openingRow['rate'];?></td>
            <td><?php echo $openingRow['item_name'];?></td>
            <td><?php echo $openingRow['client_code'];?> (<?php echo $openingRow['client_name'];?>)</td>
            <td><?php echo date("d-m-Y", $openingRow['createdon'])." ".$openingRow['actiontime'];?></td>
            <td><?php echo $openingRow['id'];?></td>
		</tr>
		<?php
	}
}
function getClosing($client_code, $actiondate, $item_code) {
	global $conn;
	$getClosingQ = "SELECT * FROM `soda_info` WHERE `soda_status` = '1'  AND `item_code` LIKE '".$item_code . "' AND `client_code` LIKE '".$client_code . "' AND `actiondate` LIKE '" . $actiondate ."'";
	$getClosingR = mysqli_query($conn, $getClosingQ) or die(mysqli_error());
	while($closingRow = mysqli_fetch_assoc($getClosingR)) {
		?>
		<tr <?php if($closingRow['entry_type']=="+"){ echo "style=color:#0000FF;"; } else { echo "style=color:#FF0000;"; } ;?>>
			<?php if($get_delete=='1'){ ?><td><input type="checkbox" name="ClientInfo[]" value="<?php echo $closingRow['id']; ?>" /></td><?php } ?>
            <?php if($get_confirm=='1'){ ?><td><input type="text" class="cls_confirm" id="<?php echo $closingRow['id']; ?>"  maxlength="1" value="<?php echo $closingRow['confirm'];  ?>" /></td><?php } ?>
			<td><?php echo $closingRow['entry_type'];?></td>
            <td style="text-align:right;"><?php echo $closingRow['lot']; ?> (<?php echo $closingRow['qty'];?>)</td>
            <td style="text-align:right;"><?php echo $closingRow['rate'];?></td>
            <td><?php echo $closingRow['item_name'];?></td>
            <td><?php echo $closingRow['client_code'];?> (<?php echo $closingRow['client_name'];?>)</td>
            <td><?php echo date("d-m-Y", $closingRow['createdon'])." ".$closingRow['actiontime'];?></td>
            <td><?php echo $closingRow['id'];?></td>
		</tr>
		<?php
	}
}						
?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>Daily Soda Position Information</h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Daily Soda Position</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive" style="overflow-x: hidden;">
				<div class="col-md-12">
					<?php include("msg.php"); ?>
					<form action="" method="post" name="soda_list" id="soda_list">
						  <div class="form-group has-feedback">
							<div class="row">
								<div class="col-md-3">
                                    <label class="form-label">Select Client Name</label><br/>
                                    <select class="form-control" name="client_name">
                                        <option value="0">All</option>  
                                      <?php
											$sSQL = "SELECT * FROM client_info";
											$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
											while($row1 = mysqli_fetch_assoc($rs1)) {
												if(isset($_POST["client_name"]) && $_POST["client_name"]==$row1["client_name"]) {
													?> <option selected="" value="<?php echo $row1["client_name"]; ?>"> <?php echo $row1["client_name"]; ?> </option><?php
												} else {
													?> <option value="<?php echo $row1["client_name"]; ?>"> <?php echo $row1["client_name"]; ?> </option><?php
												}
											}
										?>
                                    </select>
								</div>
								<div class="col-md-3">
                                    <label class="form-label">Select Item Name</label><br/>
                                    <select class="form-control" name="item_name">
                                        <option value="0">All</option>  
                                      <?php		
											$sSQL = "SELECT * FROM item_info";
											$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
											while($row1 = mysqli_fetch_assoc($rs1)) {
												if(isset($_POST["item_name"]) && $_POST["item_name"]==$row1["item_name"]) {
													?> <option selected="" value="<?php echo $row1["item_name"]; ?>"/> <?php echo $row1["item_name"]; ?> </option><?php
												} else {
													?> <option value="<?php echo $row1["item_name"]; ?>"/> <?php echo $row1["item_name"]; ?> </option><?php
												}
											}
										?>
                                    </select>
								</div>
                                <div class="col-md-2">
                                    <label class="form-label">Select Shorting </label><br/>
                                    <select class="form-control" name="shorting" style="padding-right: 0px;">
                                      <option <?php if(isset($_POST['shorting']) && $_POST['shorting']==1){echo "Selected";}  ?> value="1">Sort By Time</option>
                                      <option <?php if((isset($_POST['shorting']) && $_POST['shorting']==2) || (!isset($_POST['shorting']))){echo "Selected";}  ?> value="2">Sort By Party</option>
                                      <option <?php if(isset($_POST['shorting']) && $_POST['shorting']==3){echo "Selected";}  ?> value="3">Sort By Item</option>
                                      <option <?php if(isset($_POST['shorting']) && $_POST['shorting']==4){echo "Selected";}  ?> value="4">Sort By Buy Sell</option>
                                    </select>
                                </div>
								<div class="col-md-3">
                                    <label class="form-label">Select Date</label><br/>
									<select name="currentDate" id="currentDate" style="height:34px;width:50px;">
										<?php for($i=1;$i<=31;$i++){?>
											<?php if($i < 10){ $i = '0'.$i;} ?>
											<?php if(isset($_POST['currentDate']) && $i == $_POST['currentDate']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php
											}
											else if(!isset($_POST['currentDate']) && date('d') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
									<select name="currentMonth" id="currentMonth" style="height:34px;width:50px;">
										<?php for($i=1;$i<=12;$i++){?>
											<?php if($i < 10){ $i = '0'.$i;}?>
											<?php if(isset($_POST['currentMonth']) && $i == $_POST['currentMonth']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else if(!isset($_POST['currentMonth']) && date('m') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
									<select name="currentYear" id="currentYear" style="height:34px;width:60px;">
										<?php for($i=date('Y')-2;$i<=date('Y')+2;$i++) { ?>
											<?php if(isset($_POST['currentYear']) && $i == $_POST['currentYear']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else if(!isset($_POST['currentYear']) && date('Y') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php } ?>
									</select>
								</div>
								<div class="col-md-1">
                                    <br/>
                                    <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit">Go!</button>
								</div>
							</div>
						  </div>
					</form>
				</div>
				<div class="col-xs-12">
					<?php if ($disp_soda_list == 1) { //display default soda list page ?>
                    <form method="POST" action="" id="myForm">    
						<div class="row">
	                      <table class="table table-bordered table-striped">
							<tbody>
								<tr>
	                            	
	                                <?php if($get_delete=='1'){ ?><th><input type="checkbox" onclick="selectAll();" id="All"/></th><?php } ?>
	                                <?php if($get_confirm=='1'){ ?><th>Confirm</th><?php } ?>
	                                <th>+/-</th>
	                                <th style="text-align: right;">Lot (Quantity)</th>
	                                <th style="text-align: right;">Rate</th>
	                                <th>Script</th>
	                                <th>Party Code (Party Name)</th>												
	                                <th>Date Time</th>
	                                <th>Soda No</th>
								</tr>
								<?php
									if(isset($_POST["shorting"])) {
										if($_POST["shorting"]=="1") {
											$shorting=" ORDER BY TIME(actiontime)";
										} else if($_POST["shorting"]=="2") {
											$shorting=" ORDER BY  client_name,item_name,TIME(actiontime)";
										} else if($_POST["shorting"]=="3") {
											$shorting=" ORDER BY item_name,client_name,TIME(actiontime)";
										} else if($_POST["shorting"]=="4") {
											$shorting=" ORDER BY entry_type,client_name,item_name,TIME(actiontime)";
										}
									} else {
										$shorting="ORDER BY  client_name,item_name,TIME(actiontime)";
									}

									$sSQL = "SELECT * FROM `soda_info` WHERE `soda_status` = '0' AND `actiondate` LIKE '" . $current_date1 . "'".$shorting;

									if(isset($_POST["client_name"]) && $_POST["client_name"] != "0") {
										$sSQL = "SELECT * FROM soda_info WHERE `soda_status` = '0' AND client_name = '".$_POST["client_name"]. "' AND actiondate = '" . $current_date1 ."'".$shorting;
									}
									if(isset($_POST["item_name"]) && $_POST["item_name"] != "0") {
										$sSQL = "SELECT * FROM soda_info WHERE `soda_status` = '0' AND item_name = '".$_POST["item_name"]."' AND actiondate = '" . $current_date1 ."'".$shorting;
									}
									if(isset($_POST["client_name"]) && $_POST["client_name"] != "0" && isset($_POST["item_name"]) && $_POST["item_name"] != "0") {
										$sSQL = "SELECT * FROM soda_info WHERE `soda_status` = '0' AND client_name = '".$_POST["client_name"] . "' AND item_name = '". $_POST["item_name"] ."' AND actiondate = '" . $current_date1 ."'".$shorting;
									}
									$rs = mysqli_query($conn, $sSQL) or print mysqli_error($conn);
									
									$i = 0;
									$j = 1;
									$count = mysqli_num_rows($rs);
									while($row = mysqli_fetch_assoc($rs)) {
										if($i == 0) {
											$client_code = $row['client_code'];
											$item_code   = $row['item_code'];
											getOpening($row['client_code'], $row['actiondate'], $row['item_code']);
										}
										$i++; $j++;

										if($i != 0 && ((($row['client_code'] == $client_code && $item_code != $row['item_code']) || $row['client_code'] != $client_code) || $j == $count)) {
											getClosing($client_code, $row['actiondate'], $item_code);
											$i = 0;
										}
								?>
									<tr <?php if($row['entry_type']=="+"){ echo "style=color:#0000FF;"; } else { echo "style=color:#FF0000;"; } ;?>>
										<?php if($get_delete=='1'){ ?><td><input type="checkbox" name="ClientInfo[]" value="<?php echo $row['id']; ?>" /></td><?php } ?>
	                                    <?php if($get_confirm=='1'){ ?><td><input type="text" class="cls_confirm" id="<?php echo $row['id']; ?>" maxlength="1" value="<?php echo $row['confirm'];  ?>" /></td><?php } ?>
	                                    <td><?php echo $row['entry_type'];?></td>
	                                    <td style="text-align:right;"><?php echo $row['lot']; ?> (<?php echo $row['qty'];?>)</td>
	                                    <td style="text-align:right;"><?php echo $row['rate'];?></td>
	                                    <td><?php echo $row['item_name'];?></td>
	                                    <td><?php echo $row['client_code'];?> (<?php echo $row['client_name'];?>)</td>
	                                    <td><?php echo date("d-m-Y", $row['createdon'])." ".$row['actiontime'];?></td>
	                                    <td><?php echo $row['id'];?></td>
									</tr>
								<?php } ?>
							</tbody>
						</table>
	                    </div>
	                    <?php if($get_delete=='1'){ ?>
	                    <div class="row">
	                        <div class="col-md-12">
	                            <input type="hidden" name="delete" value=""/>
	                            <button class="btn btn-danger" type="button" name="delete" onclick="SubmitForm()">Delete selected</button>
	                        </div>
	                    </div>
	                    <?php } ?>
                    </form>
                    <?php } //display default soda list page ?>
                    <div class="row">
                    	<?php if($get_brok=='1') { ?>
                        <div class="col-xs-12">
                            <hr/>
                            <h2><center>Brokerage</center></h2>
                        </div>
                    	<?php } ?>
                        <div class="row"> 
                            <div class="col-xs-12">
                                <?php
								// Select Brokerage Data Query 
                                $selectBrokerageDataQ = "SELECT `soda_info`.*, `client_info`.`brok_type`, `lot_qty`, `brok1`, `brok2`
														   FROM `soda_info`
														   JOIN `client_info` ON `client_info`.`numeric_code` = `soda_info`.`client_code`
														   JOIN `item_info` ON `item_info`.`item_code` = `soda_info`.`item_code`
														   JOIN `clientbrok` ON `item_info`.`id` = `clientbrok`.`item_id` AND `client_info`.`id` = `clientbrok`.`client_id`
														  WHERE `actiondate` LIKE '" . $current_date1 . "' AND `soda_status` = '0'
														  ORDER BY `soda_info`.`client_name`,`soda_info`.`item_name`,TIME(`soda_info`.`actiontime`)";
								$selectBrokerageDataR = mysqli_query($conn, $selectBrokerageDataQ) or print mysqli_error($conn);
								$calculate = array();
								while($row = mysqli_fetch_assoc($selectBrokerageDataR)) {
									$calculate[$row['client_code']]['brok_type'] = $row['brok_type']; // set client broke type
									$calculate[$row['client_code']]['brok1'] = $row['brok1']; // set client brok 1
									$calculate[$row['client_code']]['brok2'] = $row['brok2']; // set client brok 2
									if(!isset($calculate[$row['client_code']][$row['item_code']]['amount'])) {
										$calculate[$row['client_code']][$row['item_code']]['amount'] = 0; // initiate set item wise total
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['totalTransaction'])) {
										$calculate[$row['client_code']][$row['item_code']]['totalTransaction'] = 0; // initiate item wise total
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['sellAmount'])) {
										$calculate[$row['client_code']][$row['item_code']]['sellAmount'] = 0; // initiate item wise sell amount
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['buyAmount'])) {
										$calculate[$row['client_code']][$row['item_code']]['buyAmount'] = 0;  // initiate item wise buy amount 
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['buy'])) {
										$calculate[$row['client_code']][$row['item_code']]['buy'] = 0;  // initiate buy amount count
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['sell'])) {
										$calculate[$row['client_code']][$row['item_code']]['sell'] = 0;  // initiate sell amount count
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['totalTransactionRate'])) {
										$calculate[$row['client_code']][$row['item_code']]['totalTransactionRate'] = 0;  // initiate item wise transaction rate
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['totalTransactionAmount'])) {
										$calculate[$row['client_code']][$row['item_code']]['totalTransactionAmount'] = 0;  // initiate item wise transaction amount total
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['transactionAvarageCost'])) {
										$calculate[$row['client_code']][$row['item_code']]['transactionAvarageCost'] = 0;  // initiate item wise transaction rate avarge cost
									}
									
									// Apply Sum and avarage for above initiate value
									$calculate[$row['client_code']][$row['item_code']]['totalTransaction'] += 1;
									$calculate[$row['client_code']][$row['item_code']]['totalTransactionRate'] += ( $row['rate']);
									$calculate[$row['client_code']][$row['item_code']]['totalTransactionAmount'] += ($row['lot'] * $row['lot_qty'] * $row['rate']);
									
									if($row['brok_type'] == "Percentage") {
										$calculate[$row['client_code']][$row['item_code']]['amount'] += abs($row['lot']);
										if($row['entry_type'] == '+') {
											$calculate[$row['client_code']][$row['item_code']]['buy'] += $row['lot'];
											$calculate[$row['client_code']][$row['item_code']]['buyAmount'] += ($row['lot'] * $row['lot_qty'] * $row['rate']);;
										}
										if($row['entry_type'] == '-') {
											$calculate[$row['client_code']][$row['item_code']]['sell'] += $row['lot'];
											$calculate[$row['client_code']][$row['item_code']]['sellAmount'] += ($row['lot'] * $row['lot_qty'] * $row['rate']);;
										}
									}

									if($row['brok_type'] == "Fixed") {
											$calculate[$row['client_code']][$row['item_code']]['amount'] += $row['lot'];
										if($row['entry_type'] == '+') {
											$calculate[$row['client_code']][$row['item_code']]['buy'] += 1;
											$calculate[$row['client_code']][$row['item_code']]['buyAmount'] += ($row['lot']);
										}
										if($row['entry_type'] == '-') {
											$calculate[$row['client_code']][$row['item_code']]['sell'] += 1;
											$calculate[$row['client_code']][$row['item_code']]['sellAmount'] += ($row['lot']);
										}
									}
									$$row['client_code'] = $row['client_name'];
								}

								// Pass $calculate to caclulate_brokerage() for calculating broketage
								$brokerage = caclulate_brokerage($calculate);
								$clientWiseBrok = array();
								foreach($brokerage as $client_code => $brok) {
										$total = 0;
										$clientWiseBrok[$client_code] = $brok['intraDay'] + $brok['overNight'];
								}
                                ?>
                                <?php if($get_brok=='1') { ?>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Client Name</th>
                                            <th>Item Name</th>
                                            <th>Brokerage 1</th>
                                            <th>Brokerage 2</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                 <?php } ?>
                                 <?php
									$new_selectBrokerageDataQ = "SELECT `soda_info`.*, `client_info`.`brok_type`, `lot_qty`, `brok1`, `brok2`
													   FROM `soda_info`
													   JOIN `client_info` ON `client_info`.`numeric_code` = `soda_info`.`client_code`
													   JOIN `item_info` ON `item_info`.`item_code` = `soda_info`.`item_code`
													   JOIN `clientbrok` ON `item_info`.`id` = `clientbrok`.`item_id` AND `client_info`.`id` = `clientbrok`.`client_id`
													  WHERE `actiondate` LIKE '" . $current_date1 . "' group by item_code,client_code ORDER BY `soda_info`.`client_name`,`soda_info`.`item_name`,TIME(`soda_info`.`actiontime`)";
									$new_selectBrokerageDataR = mysqli_query($conn, $new_selectBrokerageDataQ) or print mysqli_error($conn);
									while($new_row = mysqli_fetch_assoc($new_selectBrokerageDataR)) {
										$client_brokarage_data=array();
										$brok_type=$new_row['brok_type'].'_ItemWise';
										$client_brokarage_data=$brok_type($new_row['client_code'],$new_row['brok1'],$new_row['brok2'],$calculate[$new_row['client_code']][$new_row['item_code']]);
								?>
										<?php if($get_brok=='1') { ?>
										<tr>
                                            <td><?php echo $new_row['client_name']; ?></td>
                                            <td><?php echo $new_row['item_name']; ?></td>
                                            <td><?php echo $client_brokarage_data['intraDay']; ?></td>
                                            <td><?php echo $client_brokarage_data['overNight']; ?></td>
                                            <td><?php echo $client_brokarage_data['intraDay'] + $client_brokarage_data['overNight']; ?></td>
                                        </tr>
										<?php } ?>
								<?php } ?>
                                 <?php if($get_brok=='1') { ?>       
                                    </tbody>    
                                    <tfoot>
                                        <tr>
                                            <th>Client Name</th>												
                                            <th>Item Name</th>
                                            <th>Brokerage 1</th>
                                            <th>Brokerage 2</th>
                                            <th>Total</th>
                                        </tr>
                                    </tfoot>
                                </table>
                                <?php } ?>
								<hr/>
                                <?php if($get_profitloss=='1') { ?>
								<h2><center>Profit / Loss Report</center></h2>
                                <?php } ?>
								<?php
								$selectClientWiseBrokeQ = " SELECT * FROM `soda_info` WHERE `actiondate` LIKE '" . $current_date1 . "' AND `soda_status` = '0'
														  ORDER BY `soda_info`.`client_name`,`soda_info`.`item_name`,TIME(`soda_info`.`actiontime`)";
								$selectClientWiseBrokeR = mysqli_query($conn, $selectClientWiseBrokeQ) or die(mysqli_error($conn));
								$clientItemWiseBroks = array();
								while($row = mysqli_fetch_assoc($selectClientWiseBrokeR)) {
									if(!isset($clientItemWiseBroks[$row['client_code']][$row['item_code']]['totalBuy'])) {
										$clientItemWiseBroks[$row['client_code']][$row['item_code']]['totalBuy'] = 0;
									}
									if(!isset($clientItemWiseBroks[$row['client_code']][$row['item_code']]['totalSell'])) {
										$clientItemWiseBroks[$row['client_code']][$row['item_code']]['totalSell'] = 0;
									}
									if($row['entry_type'] == '+') {
										$clientItemWiseBroks[$row['client_code']][$row['item_code']]['totalBuy'] += $row['rate'] * $row['qty'] ;
									} else {
										$clientItemWiseBroks[$row['client_code']][$row['item_code']]['totalSell'] += $row['rate'] * $row['qty'] ;
									}
									$item = "itemCode_".$row['item_code'];
									$$item = $row['item_name'];
								}
								foreach($clientItemWiseBroks as $clientCode => $clientItemWiseBrok) {
									?>
                                    <?php if($get_profitloss=='1'){ ?>
									<table class="table table-bordered table-striped">
										<tr>
											<th colspan="6"><?php echo $$clientCode; ?></th>
										</tr>
										<?php
										if(!empty($clientItemWiseBrok)) {
											?>
											<tr>
												<th>Item</th>
												<th>Buy Amount</th>
												<th>Sell Amount</th>
												<th>Brokerage</th>
												<th>Diffrent</th>
												<th>Item Wise Profit or Loss</th>
											</tr>
											<?php
										}
										$totalSell = 0;
										$totalBuy = 0;
										foreach($clientItemWiseBrok as $itemCode => $clientItemWiseBro) {
											$item = "itemCode_".$itemCode;
											$totalSell += $clientItemWiseBroks[$clientCode][$itemCode]['totalSell'];
											$totalBuy += $clientItemWiseBroks[$clientCode][$itemCode]['totalBuy'];
											$clientItemWiseBrokarageAmount = 0.00;
											$diff = $clientItemWiseBroks[$clientCode][$itemCode]['totalSell'] - $clientItemWiseBroks[$clientCode][$itemCode]['totalBuy'] - $clientItemWiseBrokarageAmount;
											?>
											<tr>
												<td><?php echo $$item; ?></td>
												<td><?php echo $clientItemWiseBroks[$clientCode][$itemCode]['totalBuy']; ?></td>
												<td><?php echo $clientItemWiseBroks[$clientCode][$itemCode]['totalSell']; ?></td>
												<td><?php echo $clientItemWiseBrokarageAmount; ?></td>
												<td><?php echo $diff; ?></td>
												<td><?php echo $diff > 0 ? "Profit" : "Loss"; ?></td>
											</tr>
											<?php
										}
										?>
										<tr>
											<th>Analayis</th>
											<th><?php echo $totalBuy; ?></th>
											<th><?php echo $totalSell; ?></th>
											<th><?php echo $clientWiseBrok[$clientCode]; ?></th>
											<th><?php echo $totalSell - $totalBuy - $clientWiseBrok[$clientCode]; ?></th>
											<th><?php echo $totalSell - $totalBuy - $clientWiseBrok[$clientCode] > 0 ? "Profit" : "Loss"; ?></th>
										</tr>
									</table>
                                    <?php } ?>
									<?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    if($get_summary=='1') {
                    	$Total=array();
                    	$TotalSql="SELECT * FROM `soda_info` WHERE `soda_status` = '0' AND `actiondate` LIKE '" . $current_date1 . "'";
						if(isset($_POST["client_name"]) && $_POST["client_name"] != "0") {
							$TotalSql="SELECT * FROM soda_info WHERE `soda_status` = '0' AND client_name = '".$_POST["client_name"]. "' AND actiondate = '" . $current_date1 ."'";
						}
						if(isset($_POST["item_name"]) && $_POST["item_name"] != "0") {
							$TotalSql="SELECT * FROM soda_info WHERE `soda_status` = '0' AND item_name = '".$_POST["item_name"]."' AND actiondate = '" . $current_date1 ."'";
						}
						if(isset($_POST["client_name"]) && $_POST["client_name"] != "0" && isset($_POST["item_name"]) && $_POST["item_name"] != "0") {
							$TotalSql="SELECT * FROM soda_info WHERE `soda_status` = '0' AND client_name = '".$_POST["client_name"] . "' AND item_name = '". $_POST["item_name"] ."' AND actiondate = '" . $current_date1 ."'";
						}

                        ## For Total Calculation 
                        $TotalResult = mysqli_query($conn, $TotalSql) or print mysqli_error($conn);
						while($row = mysqli_fetch_assoc($TotalResult)) {
                            if($row['entry_type']=="+") {
                                $Total[$row['item_name']]["Lot"]["Buy"][]=$row['lot'];
                                $Total[$row['item_name']]["Qty"]["Buy"][]=$row['qty'];
                                $Total[$row['item_name']]["Qty"]["TotalBuyRate"][]=$row['qty']*$row['rate'];
                            } else {
                                $Total[$row['item_name']]["Lot"]["Sell"][]=$row['lot'];
                                $Total[$row['item_name']]["Qty"]["Sell"][]=$row['qty'];
                                $Total[$row['item_name']]["Qty"]["TotalSellRate"][]=$row['qty']*$row['rate'];
                            }
                        }
                    ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr/>
                            <h2><center>Summary</center></h2>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-xs-12">
                          <table class="table table-bordered table-striped">
                              <tbody>
                              <tr>
                                  <th style="text-align: right">Quantity</th>
                                  <th style="text-align: right">Average</th>
                                  <th>Close</th>												
                                  <th>Turnover</th>												
                                  <th>Profit/Loss</th>												
                              </tr>
                              </tbody>    
                              <tbody>
                                  <?php foreach ($Total as $key => $ItemName) { ?>
                                  <?php
                                      $TotalLotBuy=$TotalLotSell=0;
                                      $TotalQtyBuy=$TotalQtySell=0;
                                      $TotalQtyBuyRate=$TotalQtySellRate=0;
                                      if(!empty($ItemName["Lot"]["Buy"])) { $TotalLotBuy=array_sum($ItemName["Lot"]["Buy"]); }
                                      if(!empty($ItemName["Lot"]["Sell"])) { $TotalLotSell=array_sum($ItemName["Lot"]["Sell"]); }
                                      if(!empty($ItemName["Qty"]["Buy"])) { $TotalQtyBuy=array_sum($ItemName["Qty"]["Buy"]); }
                                      if(!empty($ItemName["Qty"]["Sell"])) { $TotalQtySell=array_sum($ItemName["Qty"]["Sell"]); }
                                      if(!empty($ItemName["Qty"]["TotalBuyRate"])) { $TotalQtyBuyRate=array_sum($ItemName["Qty"]["TotalBuyRate"]); }
                                      if(!empty($ItemName["Qty"]["TotalSellRate"])) { $TotalQtySellRate=array_sum($ItemName["Qty"]["TotalSellRate"]); }
                                  ?>
                                  <tr>
                                      <th style="text-align: right"><?php echo $TotalQtyBuy+$TotalQtySell; ?></th>
                                      <th style="text-align: right"><?php if(($TotalQtyBuyRate - $TotalQtySellRate)>0){ echo ($TotalQtyBuyRate - $TotalQtySellRate)/($TotalQtyBuy - $TotalQtySell); } else { echo '0'; }?></th>
                                      <th>&nbsp;</th>												
                                      <th><?php echo ($TotalQtyBuyRate+$TotalQtySellRate); ?></th>												
                                      <th><?php echo $TotalQtySellRate - $TotalQtyBuyRate > 0 ? "Profit" : "Loss"; ?></th>
                                  </tr>
                                  <?php } ?>
                              </tbody>
                          </table>
                        </div>
                    </div>
                    <?php } ?>
				</div>
            </div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
?>

<script type="text/javascript">
  function selectAll(){
    if(document.getElementById("All").checked){
      $('input[type=checkbox]').prop('checked', true);
    }else{
      $('input[type=checkbox]').prop('checked', false);
    }
  }
  
  function SubmitForm(){
    
    var r = confirm("Are You Sure You Want to Delete?");
    if (r == true) {
      document.getElementById("myForm").submit();
        //txt = "You pressed OK!";
    } else {
        //txt = "You pressed Cancel!";
    }
  }
  $('.cls_confirm').keydown(function(e){
    if (e.keyCode == 38) {
       $(this).val("Y");   
	  	 $.ajax({
			type:  'post', 
			cache:  false ,
			url:  'aj-savedata.php',
			data:  {confirm_var :JSON.stringify($(this).val()+"_"+$(this).attr('id'))},
			success: function(resp) {
			  var myJSONObject = {"confirm_var": JSON.parse(resp)};
			  if(myJSONObject.confirm_var.length >= 1){
				for(var i=0;i < myJSONObject.confirm_var.length; i++){
				  var key=myJSONObject.confirm_var[i];
				   $(this).val(key); 
				}
			  }
			} 
		  });       
    }    
    if (e.keyCode == 40) {
       $(this).val("N");
	   $.ajax({
			type:  'post', 
			cache:  false ,
			url:  'aj-savedata.php',
			data:  {confirm_var :JSON.stringify($(this).val()+"_"+$(this).attr('id'))},
			success: function(resp) {
			  var myJSONObject = {"confirm_var": JSON.parse(resp)};
			  if(myJSONObject.confirm_var.length >= 1){
				for(var i=0;i < myJSONObject.confirm_var.length; i++){
				  var key=myJSONObject.confirm_var[i];
				   $(this).val(key); 
				}
			  }
			} 
		  }); 
    }
	 
});
</script>
  