<?php
session_start(); if(isset($_SESSION["logged_user_name"])) {
if(isset($_POST['submit'])) {
    $current_date1 	= $_POST['currentDate']."/".$_POST['currentMonth']."/".$_POST['currentYear'];
} else {
    $current_date1 = date('d/m/Y');
}
//echo $current_date1; die;
include_once('include/config.php');	
	
## Delete Multiple Record 
if(isset($_POST["delete"])){
  //echo "<pre>";  print_r($_POST);
  if(!empty($_POST["ClientInfo"])){
    
    $GetClientInfoCSV =  implode(",", $_POST["ClientInfo"]);
    //echo $GetClientInfoCSV; 
    $sSQL = "DELETE FROM `soda_info` WHERE `id` IN (".$GetClientInfoCSV.")";
    $rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
    $_SESSION['success']="Record is deleted successfully.";
  }
}
$get_brok='0';
if(isset($_GET["brok"]) && $_GET["brok"]=='1')
{
	$get_brok='1';
}
$get_profitloss='0';
if(isset($_GET["profitloss"]) && $_GET["profitloss"]=='1')
{
	$get_profitloss='1';
}
$get_delete='0';
if(isset($_GET["delete"]) && $_GET["delete"]=='1')
{
	$get_delete='1';
}
$get_confirm='0';
if(isset($_GET["confirm"]) && $_GET["confirm"]=='1')
{
	$get_confirm='1';
}

?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Soda List Get Parameter details
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Soda List Get Parameter details</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive">
				<div class="col-md-12">
					<?php include("msg.php"); ?>
					<form action="" method="post" name="soda_list" id="soda_list">
						  <div class="form-group has-feedback">
							<div class="row">
								<div class="col-md-12">
                                    <label class="form-label">confirm</label><br/>
                                    <div>1) confirm=1 - Show textbox of confirm soda "Y/N", user can update this using Up/Down arrow key from keyboard. : <a href="soda_list2.php?confirm=1">Click Here</a><br/>2) confirm=0 - Hide textbox of confirm soda "Y/N". : <a href="soda_list2.php?confirm=0">Click Here</a></div>
								</div>
								<div class="col-md-1">
                                    <br/>
								</div>
								<div class="col-md-12">
                                    <label class="form-label">delete</label><br/>
                                    <div>1) delete=1 - Show checkbox & button for Delete Selected Soda. : <a href="soda_list2.php?delete=1">Click Here</a><br/>2) delete=0 - Hide checkbox & button for Delete Selected Soda. : <a href="soda_list2.php?delete=0">Click Here</a></div>
								</div>
								<div class="col-md-1">
                                    <br/>
								</div>
                                <div class="col-md-12">
                                    <label class="form-label">brok</label><br/>
                                    <div>1) brok=1 - Show Brokerage Section/Report. : <a href="soda_list2.php?brok=1">Click Here</a><br/>2) brok=0 - Hide Brokerage Section/Report. : <a href="soda_list2.php?brok=0">Click Here</a></div>         
                                </div>
								<div class="col-md-1">
                                    <br/>
								</div>
								<div class="col-md-12">
                                    <label class="form-label">profitloss</label><br/>
                                    <div>1) profitloss=1 - Show Profit/Loss Section/Report. : <a href="soda_list2.php?profitloss=1">Click Here</a><br/>2) profitloss=0 - Hide Profit/Loss Section/Report. : <a href="soda_list2.php?profitloss=0">Click Here</a></div>         
								</div>
								<div class="col-md-1">
                                    <br/>
								</div>
							</div>
						  </div>
					</form>
				</div>
				<div class="col-xs-12">
                    
					<div class="row">
                      

                    </div>
                    
                    <div class="row">
                        <div class="col-md-12">
                           
                        </div>
                    </div>
                    
                    
				</div>
            </div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
?>

