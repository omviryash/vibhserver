<?php
session_start(); if(isset($_SESSION["logged_user_name"]))
{
include_once('include/config.php');	
if(isset($_GET['date'])){
	$date = date('Y-m-d',strtotime($_GET['date']));
}else{
	$date = date('Y-m-d');
}
$select_closing = "SELECT 
					sum(case when (soda_info.entry_type = '+') then soda_info.qty else CONCAT('-',soda_info.qty) end) as qty,
					sum(case when (soda_info.entry_type = '+') then soda_info.rate else CONCAT('-',soda_info.rate) end) as soda_info_rate,
					case when closing_rate.amount is null then 0 else closing_rate.amount end as closing_rate_amount,
					soda_info.item_code,
					item_info.item_name,
					client_info.client_name,
					client_info.id as client_id
					from soda_info 
					inner join item_info on item_info.item_code = soda_info.item_code
					inner join client_info on client_info.numeric_code = soda_info.client_code
					left join closing_rate on closing_rate.item_id = item_info.id 
					where 1=1 and closing_rate.report_date = '".$date."' and date_format(str_to_date(soda_info.actiondate, '%d/%m/%Y'), '%Y-%m-%d') <='".$date."'".
					"group by soda_info.client_code,soda_info.item_code";
				$rs_closing = mysqli_query($conn,$select_closing) or print(mysqli_error($conn));				
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
    <style>
        .content{
          min-height: 0px; 
        }
    </style>
    <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Closing Value Detail
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Closing Value  Detail</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive">
				<div class="col-md-12">
					<?php include("msg.php"); ?>
                    <form action="closing_value_detail.php" method="get" name="closing_value_detail" id="closing_value_detail">
						<div class="form-group has-feedback">
							<div class="row">
			                    <div class="col-md-8 col-md-offset-2">
									<div class="col-md-8">
										<div class="col-md-4"><label class="form-label">Report Date :  </label></div>
										<div class="col-md-6">
											<input type="text" name="date" class="datepicker form-control" value="<?php echo date('d-m-Y',strtotime($date)); ?>">
										</div>
									</div>
									<input type="submit" value="Search" class="col-md-3 btn btn-primary btn-flat" />
								</div>
								</br></br>
							</div>
							<div class="col-md-12">
								<div class="row">
									<?php 
									$soda_info_total_rat 	= 0;
									$total_net				= 0;
									$client_id 				= 0;
									if(mysqli_num_rows($rs_closing)>0){ 
									while(($data = mysqli_fetch_assoc($rs_closing))) { 
										
										if($data['client_id'] != $client_id) {
											if($client_id != 0){ ?>
														<tr>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td><?php echo number_format($soda_info_total_rat,2,'.',','); $soda_info_total_rat = 0;?></td>
														<td>&nbsp;</td>
														<td>&nbsp;</td>
														<td><?php echo number_format($total_net,2,'.',',');$total_net = 0; ?></td>
													</tr>
												</table>
											</div>
											<?php }
											$client_id = $data['client_id'];
											
									?>
									<div class="col-md-6">
										
										<table class="table table-bordered close-report" width="100%">
											<tr>
												<th>&nbsp;</th>
												<th>&nbsp;</th>
												<th align="center" colspan="2">Average</th>
												<th align="center" colspan="2">Closing</th>
											</tr>
											<tr>
												<th>Script</th>
												<th>Qty</th>
												<th>Rate</th>
												<th>Amt</th>
												<th>Rate</th>
												<th>Net</th>
											</tr>
											<tr>
												<td>Party : <b><?php echo $data['client_name']; ?></b></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
											</tr>
										<?php } ?>
										<tr>
											<td><?php echo $data['item_name'] ?></td>
											<td><?php echo $data['qty'] ?></td>
											<td><?php $soda_info_total_rat = $soda_info_total_rat + ($data['soda_info_rate']/$data['qty']);
											echo number_format($data['soda_info_rate']/$data['qty'],2,'.',',')?></td>
											<td><?php echo number_format($data['soda_info_rate'],2,'.',','); ?></td>
											<td><?php echo number_format($data['closing_rate_amount'],2,'.',','); ?></td>
											<td><?php $total_net = $total_net + (($data['soda_info_rate']/$data['qty']) + ($data['qty']*$data['closing_rate_amount']));
											echo number_format((($data['soda_info_rate']/$data['qty']) + ($data['qty']*$data['closing_rate_amount'])),2,'.',',') ?></td>
										</tr>
										
										<?php
									} ?>
											<tr>
												<td><b>Total</b></td>
												<td>&nbsp;</td>
												<td><?php echo number_format($soda_info_total_rat,2,'.',','); $soda_info_total_rat = 0;?></td>
												<td>&nbsp;</td>
												<td>&nbsp;</td>
												<td><?php echo number_format($total_net,2,'.',',');$total_net = 0; ?></td>
											</tr>	
										</table>
									</div>
										<?php 
									} else { ?>
										<div>No closing rate found ! &nbsp;&nbsp;&nbsp;&nbsp;<a href="closing_rate.php"> Add Closing Rate </a></div>
									<?php } ?>
								</div>
							</div>
						</div>
					</form>
				</div>
            </div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
?>
<script>
$(".datepicker").datepicker({format: 'dd-mm-yyyy'});
</script>