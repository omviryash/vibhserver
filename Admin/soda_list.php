<?php
session_start(); if(isset($_SESSION["logged_user_name"])) {
if(isset($_POST['submit'])) {
    $current_date1 	= $_POST['currentDate']."/".$_POST['currentMonth']."/".$_POST['currentYear'];
} else {
    $current_date1 = date('d/m/Y');
}
if(isset($_SESSION["logged_usertype"])) {
	$usertype=$_SESSION["logged_usertype"];
}
else{
	$usertype='0';
}

//echo $current_date1; die;
include_once('include/config.php');	
	
## Delete Multiple Record 
if(isset($_POST["delete"])){
  //echo "<pre>";  print_r($_POST);
  if(!empty($_POST["ClientInfo"])){
    
    $GetClientInfoCSV =  implode(",", $_POST["ClientInfo"]);
    //echo $GetClientInfoCSV; 
    $sSQL = "DELETE FROM `soda_info` WHERE `id` IN (".$GetClientInfoCSV.")";
    $rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
    $_SESSION['success']="Record is deleted successfully.";
  }
}

?>
<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Daily Soda Position Information
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Daily Soda Position</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive">
				<div class="col-md-12">
					<?php include("msg.php"); ?>
					<form action="" method="post" name="soda_list" id="soda_list">
						  <div class="form-group has-feedback">
							<div class="row">
								<div class="col-md-3">
                                    <label class="form-label">Select Client Name</label><br/>
                                    <select class="form-control" name="client_name">
                                        <option value="0">All</option>  
                                      <?php
											$sSQL = "SELECT * FROM client_info";
											$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
											while($row1 = mysqli_fetch_array($rs1))
											{
                                              if(isset($_POST["client_name"]) && $_POST["client_name"]==$row1["client_name"]){
                                                ?> <option selected="" value="<?php echo $row1["client_name"]; ?>"> <?php echo $row1["client_name"]; ?> </option><?php
                                              }else{
                                                ?> <option value="<?php echo $row1["client_name"]; ?>"> <?php echo $row1["client_name"]; ?> </option><?php
                                              }
                                              
											}
										?>
                                    </select>
                                    <!--
                                    <input list="client_name"  placeholder="Client Name"  value="<?php if(isset($_POST["client_name"])){ echo $_POST["client_name"];} ?>" autocomplete="off">
									<datalist id="client_name">
									<option value="0"></datalist>
                                    -->
								</div>
								<div class="col-md-3">
                                    <label class="form-label">Select Item Name</label><br/>
                                    <select class="form-control" name="item_name">
                                        <option value="0">All</option>  
                                      <?php		
											$sSQL = "SELECT * FROM item_info";
											$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
											while($row1 = mysqli_fetch_array($rs1))
											{
                                              if(isset($_POST["item_name"]) && $_POST["item_name"]==$row1["item_name"]){
                                                ?> <option selected="" value="<?php echo $row1["item_name"]; ?>"/> <?php echo $row1["item_name"]; ?> </option><?php
                                              }  else {
                                                ?> <option value="<?php echo $row1["item_name"]; ?>"/> <?php echo $row1["item_name"]; ?> </option><?php
                                              }
												
											}
										?>
                                    </select>
								</div>
                                
                                <div class="col-md-3">
                                    <label class="form-label">Select Shorting </label><br/>
                                    <select class="form-control" name="shorting" style="padding-right: 0px;">
                                      <option <?php if(isset($_POST['shorting']) && $_POST['shorting']==1){echo "Selected";}  ?> value="1">Sort By Time</option>
                                      <option <?php if((isset($_POST['shorting']) && $_POST['shorting']==2) || (!isset($_POST['shorting']))){echo "Selected";}  ?> value="2">Sort By Party</option>
                                      <option <?php if(isset($_POST['shorting']) && $_POST['shorting']==3){echo "Selected";}  ?> value="3">Sort By Item</option>
                                      <option <?php if(isset($_POST['shorting']) && $_POST['shorting']==4){echo "Selected";}  ?> value="4">Sort By Buy Sell</option>
                                    </select>
                                </div>
								<div class="col-md-2">
                                    <label class="form-label">Select Date</label><br/>
									<select name="currentDate" id="currentDate" style="height:34px;width:40px;">
										<?php for($i=1;$i<=31;$i++){?>
											<?php if($i < 10){ $i = '0'.$i;} ?>
											<?php if(isset($_POST['currentDate']) && $i == $_POST['currentDate']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php
											}
											else if(!isset($_POST['currentDate']) && date('d') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
									<select name="currentMonth" id="currentMonth" style="height:34px;width:40px;">
										<?php for($i=1;$i<=12;$i++){?>
											<?php if($i < 10){ $i = '0'.$i;}?>
											<?php if(isset($_POST['currentMonth']) && $i == $_POST['currentMonth']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else if(!isset($_POST['currentMonth']) && date('m') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
									<select name="currentYear" id="currentYear" style="height:34px;width:60px;">
										<?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
											<?php if(isset($_POST['currentYear']) && $i == $_POST['currentYear']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else if(!isset($_POST['currentYear']) && date('Y') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
								</div>
								<div class="col-md-1">
                                    <br/>
                                    <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit">Go!</button>
								</div>
							</div>
						  </div>
					</form>
				</div>
				<div class="col-xs-12">
                    <form method="POST" action="" id="myForm">    
					<div class="row">
                   
                      <table class="table table-bordered table-striped">
						<tbody>
							<tr>
                                <th><input type="checkbox" onclick="selectAll();" id="All"/></th>
								<th>+/-</th>
                                <th style="text-align: right;">Lot (Quantity)</th>
                                <th style="text-align: right;">Rate</th>
                                <th>Script</th>
                                <th>Party Code (Party Name)</th>												
                                <th>Date Time</th>
                                <th>Soda No</th>
							</tr>
							
							<?php 
                            
                              $Total=array();
                              if(isset($_POST["shorting"])){
                                if($_POST["shorting"]=="1"){
                                  $shorting=" ORDER BY TIME(actiontime)";
                                }else if($_POST["shorting"]=="2"){
                                  $shorting=" ORDER BY  client_name,item_name,TIME(actiontime)";
                                }else if($_POST["shorting"]=="3"){
                                  $shorting=" ORDER BY item_name,client_name,TIME(actiontime)";
                                }else if($_POST["shorting"]=="4"){
                                  $shorting=" ORDER BY entry_type,client_name,item_name,TIME(actiontime)";
                                }
                              }else{
                                $shorting="ORDER BY  client_name,item_name,TIME(actiontime)";
                              }
                            
								$sSQL = "SELECT * FROM `soda_info` WHERE actiondate LIKE '" . $current_date1 . "'".$shorting;
                                $TotalSql="SELECT * FROM `soda_info` WHERE actiondate LIKE '" . $current_date1 . "'";
								
								if(isset($_POST["client_name"]) && $_POST["client_name"] != "0") {
									$sSQL = "SELECT * FROM soda_info where client_name = '".$_POST["client_name"]. "' && actiondate = '" . $current_date1 ."'".$shorting;
                                    $TotalSql="SELECT * FROM soda_info where client_name = '".$_POST["client_name"]. "' && actiondate = '" . $current_date1 ."'";
								}
								if(isset($_POST["item_name"]) && $_POST["item_name"] != "0") {
									$sSQL = "SELECT * FROM soda_info where item_name = '".$_POST["item_name"]."' && actiondate = '" . $current_date1 ."'".$shorting;
                                    $TotalSql="SELECT * FROM soda_info where item_name = '".$_POST["item_name"]."' && actiondate = '" . $current_date1 ."'";
								}
								if(isset($_POST["client_name"]) && $_POST["client_name"] != "0" && isset($_POST["item_name"]) && $_POST["item_name"] != "0") {
									$sSQL = "SELECT * FROM soda_info where client_name = '".$_POST["client_name"] . "' && item_name = '". $_POST["item_name"] ."' && actiondate = '" . $current_date1 ."'".$shorting;
                                    $TotalSql="SELECT * FROM soda_info where client_name = '".$_POST["client_name"] . "' && item_name = '". $_POST["item_name"] ."' && actiondate = '" . $current_date1 ."'";
								}
								$rs = mysqli_query($conn, $sSQL) or print mysqli_error($conn);
								while($row = mysqli_fetch_array($rs)) { ?>
                                    <tr <?php if($row['entry_type']=="+"){echo "style=color:#0000FF;"; }else{echo "style=color:#FF0000;";} ;?>>
                                        <td><input type="checkbox" name="ClientInfo[]" value="<?php echo $row['id']; ?>" /></td>
                                        <td><?php echo $row['entry_type'];?></td>
                                        <td style="text-align:right;"><?php echo $row['lot']; ?> (<?php echo $row['qty'];?>)</td>
                                        <td style="text-align:right;"><?php echo $row['rate'];?></td>
                                        <td><?php echo $row['item_name'];?></td>
                                        <td><?php echo $row['client_code'];?> (<?php echo $row['client_name'];?>)</td>
                                        <td><?php echo date("d-m-Y", $row['createdon'])." ".$row['actiontime'];?></td>
                                        <td><?php echo $row['id'];?></td>
                                    </tr>
                                <?php } ?>
                            <?php
                            ## For Total Calculation 
                            $TotalResult = mysqli_query($conn, $TotalSql) or print mysqli_error($conn);
							while($row = mysqli_fetch_assoc($TotalResult)) {
                                if($row['entry_type']=="+") {
                                    $Total[$row['item_name']]["Lot"]["Buy"][]=$row['lot'];
                                    $Total[$row['item_name']]["Qty"]["Buy"][]=$row['qty'];
                                    $Total[$row['item_name']]["Qty"]["TotalBuyRate"][]=$row['qty']*$row['rate'];
                                } else {
                                    $Total[$row['item_name']]["Lot"]["Sell"][]=$row['lot'];
                                    $Total[$row['item_name']]["Qty"]["Sell"][]=$row['qty'];
                                    $Total[$row['item_name']]["Qty"]["TotalSellRate"][]=$row['qty']*$row['rate'];
                                }
                            }
                            ?>
						</tbody>
					</table>
                    
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" name="delete" value=""/>
                            <button class="btn btn-danger" type="button" name="delete" onclick="SubmitForm()">Delete selected</button>
                        </div>
                    </div>
                    </form>
                    <?php 
					//this section will only visible to user who has usertype 1 in DB
					if($usertype=='1'){ ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr/>
                            <h2><center>Brokerage</center></h2>
                        </div>
                        <div class="row"> 
                            <div class="col-xs-12">
                                <?php
								function Percentage_ItemWise($clientCode,$brok1,$brok2, $calculate) {
									$brokerage = 0;
									$overNightBrokerage = 0;
									$intraDayAmount = 0;
									$intraDayBrokerage = 0;
									$overnightPercentage = ($brok2 != 0) ? $brok2 : $brok1;
									
									if(is_array($calculate)) {

										$overnightCount = abs($calculate['buy'] - $calculate['sell']);
										$intraDayCount = $calculate['amount'] - $overnightCount;
										$overnightPercentage = $overnightPercentage / 100;
										$intraDayPercentage = $brok1 / 100;
										

										$intraDayAmount = (($calculate['totalTransactionRate'] / $calculate['totalTransaction']) * 100) * $intraDayCount;
										$intraDayBrokerage = ($intraDayAmount * $intraDayPercentage) / 2 ;
										if($brok2 != 0){
											$overNightBrokerage = round(abs(($calculate['totalTransactionAmount'] - $intraDayAmount) * $overnightPercentage) / 2,2);
										}else{
											$overNightBrokerage += round(abs(($calculate['totalTransactionAmount'] - $intraDayAmount) * $overnightPercentage) / 2,2);
										}
										//$overNightBrokerage = round(abs(($calculate['totalTransactionAmount'] - $intraDayAmount) * $overnightPercentage) / 2,2);
									}
									return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
								}

								function Fixed_ItemWise($clientCode,$brok1,$brok2, $calculate) {
									$brokerage = 0;
									$overNightBrokerage = 0;
									$intraDayAmount = 0;
									$intraDayBrokerage = 0;
									$overnightPercentage = ($brok2 != 0) ? $brok2 : $brok1;
									if(is_array($calculate)) {

										$overnightCount = abs($calculate['buyAmount'] - $calculate['sellAmount']);
										$intraDayCount = $calculate['amount'] - $overnightCount;

										$intraDayBrokerage = ($intraDayCount * $brok1) / 2 ;
										if($brok2 != 0){
											$overNightBrokerage = round(abs( $overnightCount * $overnightPercentage),2);
										}else{
											$overNightBrokerage += round(abs( $overnightCount * $overnightPercentage),2);
										}
										
									}
									return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
								}
								function Percentage($clientCode, $calculate) {
									$brokerage = 0;
									$overNightBrokerage = 0;
									$intraDayAmount = 0;
									$intraDayBrokerage = 0;
									foreach($calculate as $key => $data) {
										$overnightPercentage = ($calculate['brok2'] != 0) ? $calculate['brok2'] : $calculate['brok1'];
										if(is_array($data)) {

											$overnightCount = abs($data['buy'] - $data['sell']);
											$intraDayCount = $data['amount'] - $overnightCount;
											

											$intraDayAmount = (($data['totalTransactionRate'] / $data['totalTransaction']) * 100) * $intraDayCount;
											$intraDayBrokerage += ($intraDayAmount * $calculate['brok1']) / 2 ;
											$overNightBrokerage += round(abs(($data['totalTransactionAmount'] - $intraDayAmount) * $overnightPercentage) / 2,2);
										}
									}
									return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
								}

								function Fixed($clientCode, $calculate) {
									$brokerage = 0;
									$overNightBrokerage = 0;
									$intraDayAmount = 0;
									$intraDayBrokerage = 0;
									foreach($calculate as $key => $data) {
										$overnightPercentage = ($calculate['brok2'] != 0) ? $calculate['brok2'] : $calculate['brok1'];
										if(is_array($data)) {

											$overnightCount = abs($data['buyAmount'] - $data['sellAmount']);
											$intraDayCount = $data['amount'] - $overnightCount;

											$intraDayBrokerage += ($intraDayCount * $calculate['brok1']) / 2 ;
											$overNightBrokerage += round(abs( $overnightCount * $overnightPercentage) / 2,2);
										}
									}
									return  array("intraDay" => $intraDayBrokerage,"overNight" => $overNightBrokerage);
								}
								function caclulate_broketage($calculates) {
									$brokerage = array();
									foreach($calculates as $key => $calculate) {
										$brokerage[$key] = $calculate['brok_type']($key,$calculate);
									}
									return $brokerage;
								}
                                $selectBrokerageDataQ = "SELECT `soda_info`.*, `client_info`.`brok_type`, `lot_qty`, `brok1`, `brok2`
														   FROM `soda_info`
														   JOIN `client_info` ON `client_info`.`numeric_code` = `soda_info`.`client_code`
														   JOIN `item_info` ON `item_info`.`item_code` = `soda_info`.`item_code`
														   JOIN `clientbrok` ON `item_info`.`id` = `clientbrok`.`item_id` AND `client_info`.`id` = `clientbrok`.`client_id`
														  WHERE `actiondate` LIKE '" . $current_date1 . "' ORDER BY `soda_info`.`client_name`,`soda_info`.`item_name`,TIME(`soda_info`.`actiontime`)";
								$selectBrokerageDataR = mysqli_query($conn, $selectBrokerageDataQ) or print mysqli_error($conn);
								$calculate = array();
								while($row = mysqli_fetch_assoc($selectBrokerageDataR)) {
									$calculate[$row['client_code']]['brok_type'] = $row['brok_type'];
									$calculate[$row['client_code']]['brok1'] = $row['brok1'];
									$calculate[$row['client_code']]['brok2'] = $row['brok2'];
									if(!isset($calculate[$row['client_code']][$row['item_code']]['amount'])) {
										$calculate[$row['client_code']][$row['item_code']]['amount'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['totalTransaction'])) {
										$calculate[$row['client_code']][$row['item_code']]['totalTransaction'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['sellAmount'])) {
										$calculate[$row['client_code']][$row['item_code']]['sellAmount'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['buyAmount'])) {
										$calculate[$row['client_code']][$row['item_code']]['buyAmount'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['buy'])) {
										$calculate[$row['client_code']][$row['item_code']]['buy'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['sell'])) {
										$calculate[$row['client_code']][$row['item_code']]['sell'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['totalTransactionRate'])) {
										$calculate[$row['client_code']][$row['item_code']]['totalTransactionRate'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['totalTransactionAmount'])) {
										$calculate[$row['client_code']][$row['item_code']]['totalTransactionAmount'] = 0;
									}
									if(!isset($calculate[$row['client_code']][$row['item_code']]['transactionAvarageCost'])) {
										$calculate[$row['client_code']][$row['item_code']]['transactionAvarageCost'] = 0;
									}
									$calculate[$row['client_code']][$row['item_code']]['totalTransaction'] += 1;
									$calculate[$row['client_code']][$row['item_code']]['totalTransactionRate'] += ( $row['rate']);
									$calculate[$row['client_code']][$row['item_code']]['totalTransactionAmount'] += ($row['lot'] * $row['lot_qty'] * $row['rate']);
									
									if($row['brok_type'] == "Percentage") {
										$calculate[$row['client_code']][$row['item_code']]['amount'] += abs($row['lot']);
										if($row['entry_type'] == '+') {
											$calculate[$row['client_code']][$row['item_code']]['buy'] += $row['lot'];
											$calculate[$row['client_code']][$row['item_code']]['buyAmount'] += ($row['lot'] * $row['lot_qty'] * $row['rate']);;
										}
										if($row['entry_type'] == '-') {
											$calculate[$row['client_code']][$row['item_code']]['sell'] += $row['lot'];
											$calculate[$row['client_code']][$row['item_code']]['sellAmount'] += ($row['lot'] * $row['lot_qty'] * $row['rate']);;
										}
									}

									if($row['brok_type'] == "Fixed") {
											$calculate[$row['client_code']][$row['item_code']]['amount'] += $row['lot'];
										if($row['entry_type'] == '+') {
											$calculate[$row['client_code']][$row['item_code']]['buy'] += 1;
											$calculate[$row['client_code']][$row['item_code']]['buyAmount'] += ($row['lot']);
										}
										if($row['entry_type'] == '-') {
											$calculate[$row['client_code']][$row['item_code']]['sell'] += 1;
											$calculate[$row['client_code']][$row['item_code']]['sellAmount'] += ($row['lot']);
										}
									}
									$$row['client_code'] = $row['client_name'];
								}
								$brokerage = caclulate_broketage($calculate);
                                ?>
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Client Name</th>
                                            <th>Item Name</th>
                                            <th>Brokerage 1</th>
                                            <th>Brokerage 2</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php
										$new_selectBrokerageDataQ = "SELECT `soda_info`.*, `client_info`.`brok_type`, `lot_qty`, `brok1`, `brok2`
														   FROM `soda_info`
														   JOIN `client_info` ON `client_info`.`numeric_code` = `soda_info`.`client_code`
														   JOIN `item_info` ON `item_info`.`item_code` = `soda_info`.`item_code`
														   JOIN `clientbrok` ON `item_info`.`id` = `clientbrok`.`item_id` AND `client_info`.`id` = `clientbrok`.`client_id`
														  WHERE `actiondate` LIKE '" . $current_date1 . "' group by item_code,client_code ORDER BY `soda_info`.`client_name`,`soda_info`.`item_name`,TIME(`soda_info`.`actiontime`)";
										$new_selectBrokerageDataR = mysqli_query($conn, $new_selectBrokerageDataQ) or print mysqli_error($conn);
										while($new_row = mysqli_fetch_assoc($new_selectBrokerageDataR)) {
											$client_brokarage_data=array();
											$brok_type=$new_row['brok_type'].'_ItemWise';
											$client_brokarage_data=$brok_type($new_row['client_code'],$new_row['brok1'],$new_row['brok2'],$calculate[$new_row['client_code']][$new_row['item_code']]);
										?>
                                        	<tr>
                                                <td><?php echo $new_row['client_name']; ?></td>
                                                <td><?php echo $new_row['item_name']; ?></td>
                                                <td><?php echo $client_brokarage_data['intraDay']; ?></td>
                                                <td><?php echo $client_brokarage_data['overNight']; ?></td>
                                                <td><?php echo $client_brokarage_data['intraDay'] + $client_brokarage_data['overNight']; ?></td>
                                            </tr>
                                        <?php 
										}
										?>
										<?php 
										/*foreach($brokerage as $client_code => $brok) {
										$total = 0;
										?>
                                        <tr>
                                            <td><?php echo $$client_code; ?></td>
                                            <td><?php echo $brok['intraDay']; ?></td>
                                            <td><?php echo $brok['overNight']; ?></td>
                                            <td><?php echo $brok['intraDay'] + $brok['overNight']; ?></td>
                                        </tr>
										<?php
										}*/
										?>
                                    </tbody>    
                                    <tfoot>
                                        <tr>
                                            <th>Client Name</th>												
                                            <th>Item Name</th>
                                            <th>Brokerage 1</th>
                                            <th>Brokerage 2</th>
                                            <th>Total</th>
                                        </tr>
                                    </tfoot>
                                </table>    

                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <hr/>
                            <h2><center>Summary</center></h2>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-xs-6 col-xs-offset-3">
                          <table class="table table-bordered">
                              <tbody>
                                  <tr>
                                      <th>Item Name</th>												
                                      <th style="text-align: right" width="25%">Total Lot</th>
                                      <th style="text-align: right" width="25%">Total Quantity</th>
                                  </tr>
                              </tbody>    
                              <tbody>
                                  <?php 
                                  foreach ($Total as $key => $ItemName) {
                                    ?>
                                  <?php
                                      $TotalLotBuy=$TotalLotSell=0;
                                      $TotalQtyBuy=$TotalQtySell=0;
                                      
                                      if(!empty($ItemName["Lot"]["Buy"])){$TotalLotBuy=array_sum($ItemName["Lot"]["Buy"]); }
                                      if(!empty($ItemName["Lot"]["Sell"])){$TotalLotSell=array_sum($ItemName["Lot"]["Sell"]); }
                                      if(!empty($ItemName["Qty"]["Buy"])){$TotalQtyBuy=array_sum($ItemName["Qty"]["Buy"]); }
                                      if(!empty($ItemName["Qty"]["Sell"])){$TotalQtySell=array_sum($ItemName["Qty"]["Sell"]); }
                                      
                                  ?>
                                  <tr>
                                      <td><?php echo $key; ?></td>
                                      <td align="right"><?php echo  $TotalLotBuy - $TotalLotSell; ?></td>
                                      <td align="right"><?php echo $TotalQtyBuy - $TotalQtySell; ?></td>
                                  </tr>
                                  <?php }  ?>
                              </tbody>
                          </table>
                          </div>
                    </div>
                     <div class="row">
                        <div class="col-xs-12">
                            <hr/>
                            <h2><center>Average</center></h2>
                        </div>
                    </div>
                    <div class="row"> 
                        <div class="col-xs-6 col-xs-offset-3">
                          <table class="table table-bordered">
                              <tbody>
                                  <tr>
                                      <th>Item Name</th>												
                                      <th style="text-align: right" width="25%">Total Lot</th>
<!--                                      <th style="text-align: right" width="25%">Total Quantity</th>-->
                                      <th style="text-align: right" width="25%">Average Rate </th>
                                  </tr>
                              </tbody>    
                              <tbody>
                                  <?php 
                                  foreach ($Total as $key => $ItemName) {
                                      $TotalLotBuy=$TotalLotSell=0;
                                      $TotalQtyBuy=$TotalQtySell=0;
                                      $TotalQtyBuyRate=$TotalQtySellRate=0;
                                      
                                      if(!empty($ItemName["Lot"]["Buy"])){$TotalLotBuy=array_sum($ItemName["Lot"]["Buy"]); }
                                      if(!empty($ItemName["Lot"]["Sell"])){$TotalLotSell=array_sum($ItemName["Lot"]["Sell"]); }
                                      if(!empty($ItemName["Qty"]["Buy"])){$TotalQtyBuy=array_sum($ItemName["Qty"]["Buy"]); }
                                      if(!empty($ItemName["Qty"]["Sell"])){$TotalQtySell=array_sum($ItemName["Qty"]["Sell"]); }
                                      if(!empty($ItemName["Qty"]["TotalBuyRate"])){$TotalQtyBuyRate=array_sum($ItemName["Qty"]["TotalBuyRate"]); }
                                      if(!empty($ItemName["Qty"]["TotalSellRate"])){$TotalQtySellRate=array_sum($ItemName["Qty"]["TotalSellRate"]); }
                                  ?>
                                  <tr>
                                      <td><?php echo $key; ?></td>
                                      <td align="right"><?php echo  $TotalLotBuy - $TotalLotSell; ?></td>
<!--                                      <td align="right"><?php echo $TotalQtyBuy - $TotalQtySell; ?></td>-->
                                      <td align="right"><?php if(($TotalQtyBuy - $TotalQtySell)!=0){echo ($TotalQtyBuyRate - $TotalQtySellRate)/($TotalQtyBuy - $TotalQtySell); } else{ echo '0';} ?></td>
                                  </tr>
                                  <?php }  ?>
                              </tbody>
                          </table>
                          </div>
                    </div>
                    
                    <?php //echo "<pre>"; print_r($Total); ?>
				</div>
            </div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
?>

<script type="text/javascript">
  function selectAll(){
    if(document.getElementById("All").checked){
      $('input[type=checkbox]').prop('checked', true);
    }else{
      $('input[type=checkbox]').prop('checked', false);
    }
  }
  
  function SubmitForm(){
    
    var r = confirm("Are You Sure You Want to Delete?");
    if (r == true) {
      document.getElementById("myForm").submit();
        //txt = "You pressed OK!";
    } else {
        //txt = "You pressed Cancel!";
    }
  }
</script>
  