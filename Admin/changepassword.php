<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Change Password
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Change Password</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body">
				<div class="col-xs-6">
					<?php include("msg.php"); ?>
					<form action="dochangepassword.php" method="post" name="login" id="login">
						<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION["logged_user_id"]; ?>">
						  <div class="form-group has-feedback">
							<input type="password" class="form-control" placeholder="Old Password" name="oldpassword"/>
							<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						  </div>
						  <div class="form-group has-feedback">
							<input type="password" class="form-control" placeholder="New Password" name="newpassword"/>
							<span class="glyphicon glyphicon-lock form-control-feedback"></span>
						  </div>
						  <div class="form-group has-feedback">
							<input type="password" class="form-control" placeholder="Retype password" name="repassword" />
							<span class="glyphicon glyphicon-log-in form-control-feedback"></span>
						  </div>
						<div class="row">
							<div class="col-xs-12">
								<button type="submit" class="btn btn-primary btn-block btn-flat">Change Password</button>
							</div><!-- /.col -->
						</div>
					</form>
				</div>
			</div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
