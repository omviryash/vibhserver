<?php if(!isset($_SESSION)) {
		session_start();
}		?>
<meta charset="UTF-8">
<title>&#2384; | Dashboard</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.4 -->
<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- Font Awesome Icons -->
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- jvectormap -->
<link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
<!-- AdminLTE Skins. Choose a skin from the css/skins
	 folder instead of downloading all of them to reduce the load. -->
<link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
<link href="./plugins/craftpipjquery/jquery-confirm.min.css" rel="stylesheet" type="text/css"/>
<link href="./plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css"/>

<!-- Sweet alert style -->
        <link href="css/sweetalert.css" rel="stylesheet" type="text/css" />
		<script src="js/sweetalert.min.js"></script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script type="text/javascript">
function delete_client(id)
			{
				sweetAlert({
					title: "",
					text: "Are you sure you want to delete?",
					type: "",
					showCancelButton: true,
					confirmButtonColor: "#009999",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(response){
					if(response == true)
					{
						window.location.href = "createClient.php?mode=2&id="+id; 
					}
					else
					{
						 sweetAlert("Cancelled", "Your file is safe :)", "error");
					}
				});
			}
function delete_item(id)
			{
				sweetAlert({
					title: "",
					text: "Are you sure you want to delete?",
					type: "",
					showCancelButton: true,
					confirmButtonColor: "#009999",
					confirmButtonText: "OK",
					cancelButtonText: "Cancel",
					closeOnConfirm: true,
					closeOnCancel: true
				},
				function(response){
					if(response == true)
					{
						window.location.href = "createItem.php?mode=2&id="+id; 
					}
					else
					{
						 sweetAlert("Cancelled", "Your file is safe :)", "error");
					}
				});
			}
</script>