<?php

session_start(); if(isset($_SESSION["logged_user_name"]))
{ 
	include_once('include/config.php');	
	
if(isset($_GET['mode']) && isset($_GET['id']))
{
	if($_GET['mode']==1)
	{
		$sSQL = "SELECT * FROM item_info where id=".$_GET['id'];
		$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
		if(mysqli_num_rows($rs1) > 0)
		{
			$row1 = mysqli_fetch_array($rs1);
		}
	}
	else
	{
		$sSQL = "delete from item_info where id =".$_GET['id'];
		$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:createItem.php");exit;
	}
}
if(isset($_POST['ok']))
{
	$item_code = $_POST['item_code'];
	$item_name = $_POST['item_name'];
	$lot_qty = $_POST['lot_qty'];
	
	if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1)
	{
		//update query when required
    }
	else
	{
       	// insert code for all text boxes in client_info table
		
		$sSQL = "insert into item_info (item_code,item_name,lot_qty) values ('$item_code','$item_name','$lot_qty')";
		if(mysqli_query($conn, $sSQL))
		{
			//echo "New record created successfully";
		}
		else
		{
			echo mysqli_error($conn);
			die;
		}
		$_SESSION['success']="Record is inserted.";
	}
	mysqli_close($conn);
	header("Location:createItem.php");
    exit;
}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Item Information
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Item Information</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive">
				<div class="col-xs-6">
					<?php include("msg.php"); ?>
					<form action="" method="post" name="item_info" id="item_info">
						  <div class="form-group has-feedback">
							<input type="text" class="form-control" placeholder="Item Code" name="item_code"/>
						  </div>
						  <div class="form-group has-feedback">
							<input type="text" class="form-control" placeholder="Item Name" name="item_name"/>
						  </div>
						  <div class="form-group has-feedback">
							<input type="text" class="form-control" placeholder="Lot Quantity" name="lot_qty"/>
						  </div>
						  <div class="form-group has-feedback">
							<input type="text" class="form-control" placeholder="Price Range Start" name="price_start"/>
						  </div>
						  <div class="form-group has-feedback">
							<input type="text" class="form-control" placeholder="Price Range End" name="price_start"/>
						  </div>
						<div class="row">
							<div class="col-xs-6">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Create Item</button>
							</div>
							<div class="col-xs-6">
								<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createItem.php'"/>Reset</button>
							</div>
							<!-- /.col -->
						</div>
					</form>
				</div>
				<div class="col-xs-6">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>Action</th>
								<th>Item Code</th>												
								<th>Item Name</th>
								<th>Lot Quantity</th>
							</tr>
							
							<?php 
								$sSQL = "SELECT * from item_info ORDER BY item_name";
								$rs = mysqli_query($conn, $sSQL) or print mysqli_error($conn);
								while($row = mysqli_fetch_array($rs)){?>
							<tr>
								<!--<td><a href="createItem.php?id=<?php echo $row['id'];?>&mode=1">Edit</a> | <a href="createItem.php?id=<?php //echo $row['id'];?>&mode=2">Delete</a>-->
								<td><a href="javascript:delete_item(<?php echo $row['id']; ?>)">Delete</a></td>
								<td><?php echo $row['item_code'];?></td>
								<td><?php echo $row['item_name'];?></td>
								<td><?php echo $row['lot_qty']; ?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
