<?php
session_start();
if (isset($_SESSION["logged_user_name"])) {
    if (isset($_GET['submit'])) {
        $current_date1 = $_GET['currentDate'] . "/" . $_GET['currentMonth'] . "/" . $_GET['currentYear'];
    } else {
        $current_date1 = date('d/m/Y');
    }
    //echo $current_date1; die;
    include_once('include/config.php');

## Delete Multiple Record 
    if (isset($_GET["delete"])) {
        //echo "<pre>";  print_r($_GET);
        if (!empty($_GET["ClientInfo"])) {

            $GetClientInfoCSV = implode(",", $_GET["ClientInfo"]);
            //echo $GetClientInfoCSV; 
            $sSQL = "delete from soda_info where id IN (" . $GetClientInfoCSV . ")";
            $rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
            $_SESSION['success'] = "Record is deleted successfully.";
        }
    }
    ?>

    <!DOCTYPE html>
    <html>
        <head>
            <?php include("include/header.php"); ?>
        </head>
        <body class="skin-blue sidebar-mini">
            <div class="wrapper">
                <header class="main-header">
                    <?php include("include/mainheader.php"); ?>
                </header>
                <!-- Left side column. contains the logo and sidebar -->
                <aside class="main-sidebar">
                    <!-- sidebar: style can be found in sidebar.less -->
                    <section class="sidebar">
                        <!-- Sidebar user panel -->
                        <?php include("include/leftsidebar.php"); ?>
                    </section>
                    <!-- /.sidebar -->
                </aside>

                <!-- Content Wrapper. Contains page content -->
                <div class="content-wrapper">
                    <!-- Content Header (Page header) -->
                    <section class="content-header">
                        <h1>
                            Daily Soda Position Information
                            <!--<small>Version 2.0</small>-->
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
                            <li class="active">Daily Soda Position</li>
                        </ol>
                    </section>
                    <!-- Main content -->
                    <section class="content">
                        <!-- write content here -->
                        <div class="box-body table-responsive">
                            <div class="col-md-12">
                                <?php include("msg.php"); ?>
                                <form action="" method="GET" name="soda_list" id="soda_list">
                                    <div class="form-group has-feedback">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label class="form-label">Select Client Name</label><br/>
                                                <select class="form-control" name="client_name">
                                                    <option value="0">All</option>  
                                                    <?php
                                                    $sSQL = "SELECT * FROM client_info";
                                                    $rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
                                                    while ($row1 = mysqli_fetch_array($rs1)) {
                                                        if (isset($_GET["client_name"]) && $_GET["client_name"] == $row1["client_name"]) {
                                                            ?> <option selected="" value="<?php echo $row1["client_name"]; ?>"> <?php echo $row1["client_name"]; ?> </option><?php
                                                        } else {
                                                            ?> <option value="<?php echo $row1["client_name"]; ?>"> <?php echo $row1["client_name"]; ?> </option><?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                
                                            </div>
                                            <div class="col-md-3">
                                                <label class="form-label">Select Item Name</label><br/>
                                                <select class="form-control" name="item_name">
                                                    <option value="0">All</option>  
                                                    <?php
                                                    $sSQL = "SELECT * FROM item_info";
                                                    $rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
                                                    while ($row1 = mysqli_fetch_array($rs1)) {
                                                        if (isset($_GET["item_name"]) && $_GET["item_name"] == $row1["item_name"]) {
                                                            ?> <option selected="" value="<?php echo $row1["item_name"]; ?>"/> <?php echo $row1["item_name"]; ?> </option><?php
                                                        } else {
                                                            ?> <option value="<?php echo $row1["item_name"]; ?>"/> <?php echo $row1["item_name"]; ?> </option><?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                               
                                            </div>

                                            <div class="col-md-3">
                                                <label class="form-label">Select Shorting </label><br/>
                                                <select class="form-control" name="shorting" style="padding-right: 0px;">
                                                    <option <?php
                                                    if (isset($_GET['shorting']) && $_GET['shorting'] == 2) {
                                                        echo "Selected";
                                                    }
                                                    ?> value="2">Sort By Party</option>
                                                    <!--
                                                    <option <?php
                                                    if (isset($_GET['shorting']) && $_GET['shorting'] == 1) {
                                                        echo "Selected";
                                                    }
                                                    ?> value="1">Sort By Time</option>
                                                    <option <?php
                                                    if (isset($_GET['shorting']) && $_GET['shorting'] == 3) {
                                                        echo "Selected";
                                                    }
                                                    ?> value="3">Sort By Item</option>
                                                    <option <?php
                                                    if (isset($_GET['shorting']) && $_GET['shorting'] == 4) {
                                                        echo "Selected";
                                                    }
                                                    ?> value="4">Sort By Buy Sell</option>
                                                    -->
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <label class="form-label">Select Date</label><br/>
                                                <select name="currentDate" id="currentDate" style="height:34px;width:40px;">
                                                    <?php for ($i = 1; $i <= 31; $i++) { ?>
                                                        <?php
                                                        if ($i < 10) {
                                                            $i = '0' . $i;
                                                        }
                                                        ?>
                                                        <?php if (isset($_GET['currentDate']) && $i == $_GET['currentDate']) { ?>
                                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                            <?php
                                                        } else if (!isset($_GET['currentDate']) && date('d') == $i) {
                                                            ?>
                                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <select name="currentMonth" id="currentMonth" style="height:34px;width:40px;">
                                                    <?php for ($i = 1; $i <= 12; $i++) { ?>
                                                        <?php
                                                        if ($i < 10) {
                                                            $i = '0' . $i;
                                                        }
                                                        ?>
                                                        <?php if (isset($_GET['currentMonth']) && $i == $_GET['currentMonth']) { ?>
                                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                        <?php } else if (!isset($_GET['currentMonth']) && date('m') == $i) { ?>
                                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                                <select name="currentYear" id="currentYear" style="height:34px;width:60px;">
                                                    <?php for ($i = date('Y') - 2; $i <= date('Y') + 2; $i++) { ?>
                                                        <?php if (isset($_GET['currentYear']) && $i == $_GET['currentYear']) { ?>
                                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                        <?php } else if (!isset($_GET['currentYear']) && date('Y') == $i) { ?>
                                                            <option value="<?php echo $i; ?>" selected="selected"><?php echo $i; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <div class="col-md-1">
                                                <br/>
                                                <button type="submit" class="btn btn-primary btn-block btn-flat" name="submit">Go!</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-xs-12">
                                <form method="POST" action="" id="myForm">    
                                    <div class="row">

                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th><input type="checkbox" onclick="selectAll();" id="All"/></th>
                                                    <th>Client Name</th>
                                                    <th>Item Name</th>												
                                                    <th>Buy/Sell</th>												
                                                    <th style="text-align: right;"> Lot</th>
                                                    <th style="text-align: right;">Quantity</th>
                                                    <th style="text-align: right;">Rate</th>
                                                    <th>Date Time</th>
                                                </tr>

                                                <?php
                                                $Total = array();
                                                if (isset($_GET["shorting"])) {
                                                    if ($_GET["shorting"] == "1") {
                                                        $shorting = " ORDER BY TIME(actiontime)";
                                                    } else if ($_GET["shorting"] == "2") {
                                                        $shorting = " ORDER BY  client_name,item_name,TIME(actiontime)";
                                                    } else if ($_GET["shorting"] == "3") {
                                                        $shorting = " ORDER BY item_name,client_name,TIME(actiontime)";
                                                    } else if ($_GET["shorting"] == "4") {
                                                        $shorting = " ORDER BY entry_type,client_name,item_name,TIME(actiontime)";
                                                    }
                                                } else {
                                                    $shorting = " ORDER BY  client_name,item_name,TIME(actiontime)";
                                                }

                                                $sSQL = "SELECT * from soda_info where actiondate = '" . $current_date1 . "'" . $shorting;
                                                $TotalSql = "SELECT * from soda_info where actiondate = '" . $current_date1 . "'";
                                                //$sSQL = "SELECT * from soda_info";
                                                if (isset($_GET["client_name"]) && $_GET["client_name"] != "0") {
                                                    $sSQL = "SELECT * FROM soda_info where client_name = '" . $_GET["client_name"] . "' && actiondate = '" . $current_date1 . "'" . $shorting;
                                                    $TotalSql = "SELECT * FROM soda_info where client_name = '" . $_GET["client_name"] . "' && actiondate = '" . $current_date1 . "'";
                                                }
                                                if (isset($_GET["item_name"]) && $_GET["item_name"] != "0") {
                                                    $sSQL = "SELECT * FROM soda_info where item_name = '" . $_GET["item_name"] . "' && actiondate = '" . $current_date1 . "'" . $shorting;
                                                    $TotalSql = "SELECT * FROM soda_info where item_name = '" . $_GET["item_name"] . "' && actiondate = '" . $current_date1 . "'";
                                                }
                                                if (isset($_GET["client_name"]) && $_GET["client_name"] != "0" && isset($_GET["item_name"]) && $_GET["item_name"] != "0") {
                                                    $sSQL = "SELECT * FROM soda_info where client_name = '" . $_GET["client_name"] . "' && item_name = '" . $_GET["item_name"] . "' && actiondate = '" . $current_date1 . "'" . $shorting;
                                                    $TotalSql = "SELECT * FROM soda_info where client_name = '" . $_GET["client_name"] . "' && item_name = '" . $_GET["item_name"] . "' && actiondate = '" . $current_date1 . "'";
                                                }
                                                $rs = mysqli_query($conn, $sSQL) or print mysqli_error($conn);
                                                $ClientName = "";
                                                $ItemName = "";
                                                $TotalBuy="";
                                                while ($row = mysqli_fetch_array($rs)) {
                                                    ?>

                                                    <?php
                                                    if (empty($ClientName) && empty($ItemName)) {
                                                        $ClientName = $row['client_name'];
                                                        $ItemName = $row['item_name'];
                                                        $BuySell = $row['entry_type'];
                                                        $TotalBuy +=$row["rate"];
                                                    } else if ($ClientName != $row['client_name'] || $ItemName != $row['item_name'] || $BuySell != $row['entry_type']) {
                                                        $ClientName = $row['client_name'];
                                                        $ItemName = $row['item_name'];
                                                        $BuySell = $row['entry_type'];
                                                        $TotalBuy +=$row["rate"];
                                                        ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td >Total</td>
                                                            <td width="25%">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        Total Buy<br/><input type="text" class="form-control" value="<?php echo $TotalBuy; ?>">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        Total Sell<br/><input type="text"  class="form-control">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td width="10%">
                                                                Net Qty<br/><input type="text" class="form-control">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td ></td>
                                                            <td width="25%">
                                                                <div class="row">
                                                                    <div class="col-md-6">
                                                                        Profit Loss<br/><input type="text" class="form-control">
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        Brok<br/><input type="text"  class="form-control">
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td width="10%">
                                                                Net Profit Loss<br/><input type="text" class="form-control">
                                                            </td>
                                                        </tr>
                                                        <?php
                                                    }
                                                    ?>

                                                    <tr <?php
                                                    if ($row['entry_type'] == "+") {
                                                        echo "style=color:#0000FF;";
                                                    } else {
                                                        echo "style=color:#FF0000;";
                                                    };
                                                    ?>>
                                                            <!--<td><a href="createClient.php?id=<?php echo $row['id']; ?>&mode=1">Edit</a> | <a href="createClient.php?id=<?php //echo $row['id'];  ?>&mode=2">Delete</a>-->
                                                            <!--<td><a href="javascript:delete_client(<?php echo $row['id']; ?>)">Delete</a></td>-->
                                                        <td><input type="checkbox" name="ClientInfo[]" value="<?php echo $row['id']; ?>" /></td>
                                                        <td><?php echo $row['client_name']; ?></td>
                                                        <td><?php echo $row['item_name']; ?></td>
                                                        <td><?php
                                                            if ($row['entry_type'] == "+") {
                                                                echo "Buy";
                                                            } else {
                                                                echo "Sell";
                                                            };
                                                            ?></td>
                                                        <td style="text-align:right;"><?php echo $row['lot']; ?></td>
                                                        <td style="text-align:right;"><?php echo $row['qty']; ?></td>
                                                        <td style="text-align:right;"><?php echo $row['rate']; ?></td>
                                                        <td><?php echo date("d-m-Y", $row['createdon']) . " " . $row['actiontime']; ?></td>
                                                    </tr>

                                                <?php } ?>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td >Total</td>
                                                    <td width="25%">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Total Buy<br/><input type="text" class="form-control" value="">
                                                            </div>
                                                            <div class="col-md-6">
                                                                Total Sell<br/><input type="text"  class="form-control">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td width="10%">
                                                        Net Qty<br/><input type="text" class="form-control">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                    <td ></td>
                                                    <td width="25%">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                Profit Loss<br/><input type="text" class="form-control">
                                                            </div>
                                                            <div class="col-md-6">
                                                                Brok<br/><input type="text"  class="form-control">
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td width="10%">
                                                        Net Profit Loss<br/><input type="text" class="form-control">
                                                    </td>
                                                </tr>
                                                <?php
                                                ## For Total Calculation 
                                                $TotalResult = mysqli_query($conn, $TotalSql) or print mysqli_error($conn);
                                                while ($row = mysqli_fetch_array($TotalResult)) {
                                                    if ($row['entry_type'] == "+") {
                                                        $Total[$row['item_name']]["Lot"]["Buy"][] = $row['lot'];
                                                        $Total[$row['item_name']]["Qty"]["Buy"][] = $row['qty'];
                                                        $Total[$row['item_name']]["Qty"]["TotalBuyRate"][] = $row['qty'] * $row['rate'];
                                                    } else {
                                                        $Total[$row['item_name']]["Lot"]["Sell"][] = $row['lot'];
                                                        $Total[$row['item_name']]["Qty"]["Sell"][] = $row['qty'];
                                                        $Total[$row['item_name']]["Qty"]["TotalSellRate"][] = $row['qty'] * $row['rate'];
                                                    }
                                                }
                                                ?>

                                            </tbody>
                                        </table>

                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <input type="hidden" name="delete" value=""/>
                                            <button class="btn btn-danger" type="button" name="delete" onclick="SubmitForm()">Delete selected</button>
                                        </div>
                                    </div>
                                </form>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr/>
                                        <h2><center>Summary</center></h2>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-xs-6 col-xs-offset-3">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Item Name</th>												
                                                    <th style="text-align: right" width="25%">Total Lot</th>
                                                    <th style="text-align: right" width="25%">Total Quantity</th>
                                                </tr>
                                            </tbody>    
                                            <tbody>
                                                <?php
                                                foreach ($Total as $key => $ItemName) {
                                                    ?>
                                                    <?php
                                                    $TotalLotBuy = $TotalLotSell = 0;
                                                    $TotalQtyBuy = $TotalQtySell = 0;

                                                    if (!empty($ItemName["Lot"]["Buy"])) {
                                                        $TotalLotBuy = array_sum($ItemName["Lot"]["Buy"]);
                                                    }
                                                    if (!empty($ItemName["Lot"]["Sell"])) {
                                                        $TotalLotSell = array_sum($ItemName["Lot"]["Sell"]);
                                                    }
                                                    if (!empty($ItemName["Qty"]["Buy"])) {
                                                        $TotalQtyBuy = array_sum($ItemName["Qty"]["Buy"]);
                                                    }
                                                    if (!empty($ItemName["Qty"]["Sell"])) {
                                                        $TotalQtySell = array_sum($ItemName["Qty"]["Sell"]);
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $key; ?></td>
                                                        <td align="right"><?php echo $TotalLotBuy - $TotalLotSell; ?></td>
                                                        <td align="right"><?php echo $TotalQtyBuy - $TotalQtySell; ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <hr/>
                                        <h2><center>Average</center></h2>
                                    </div>
                                </div>
                                <div class="row"> 
                                    <div class="col-xs-6 col-xs-offset-3">
                                        <table class="table table-bordered">
                                            <tbody>
                                                <tr>
                                                    <th>Item Name</th>												
                                                    <th style="text-align: right" width="25%">Total Lot</th>
              <!--                                      <th style="text-align: right" width="25%">Total Quantity</th>-->
                                                    <th style="text-align: right" width="25%">Average Rate </th>
                                                </tr>
                                            </tbody>    
                                            <tbody>
                                                <?php
                                                foreach ($Total as $key => $ItemName) {
                                                    $TotalLotBuy = $TotalLotSell = 0;
                                                    $TotalQtyBuy = $TotalQtySell = 0;
                                                    $TotalQtyBuyRate = $TotalQtySellRate = 0;

                                                    $TotalQtyBuySell = $TotalQtyBuyRate - $TotalQtySellRate;
                                                    $TotalQty = $TotalQtyBuy - $TotalQtySell;
                                                    if ($TotalQty == "0") {
                                                        $TotalQty = 1;
                                                    }

                                                    if (!empty($ItemName["Lot"]["Buy"])) {
                                                        $TotalLotBuy = array_sum($ItemName["Lot"]["Buy"]);
                                                    }
                                                    if (!empty($ItemName["Lot"]["Sell"])) {
                                                        $TotalLotSell = array_sum($ItemName["Lot"]["Sell"]);
                                                    }
                                                    if (!empty($ItemName["Qty"]["Buy"])) {
                                                        $TotalQtyBuy = array_sum($ItemName["Qty"]["Buy"]);
                                                    }
                                                    if (!empty($ItemName["Qty"]["Sell"])) {
                                                        $TotalQtySell = array_sum($ItemName["Qty"]["Sell"]);
                                                    }
                                                    if (!empty($ItemName["Qty"]["TotalBuyRate"])) {
                                                        $TotalQtyBuyRate = array_sum($ItemName["Qty"]["TotalBuyRate"]);
                                                    }
                                                    if (!empty($ItemName["Qty"]["TotalSellRate"])) {
                                                        $TotalQtySellRate = array_sum($ItemName["Qty"]["TotalSellRate"]);
                                                    }
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $key; ?></td>
                                                        <td align="right"><?php echo $TotalLotBuy - $TotalLotSell; ?></td>
                  <!--                                      <td align="right"><?php echo $TotalQtyBuy - $TotalQtySell; ?></td>-->
                                                        <td align="right"><?php echo ($TotalQtyBuyRate - $TotalQtySellRate) / ($TotalQtyBuy - $TotalQtySell); ?></td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <?php //echo "<pre>"; print_r($Total);   ?>
                            </div>
                        </div>
                    </section><!-- /.content -->
                </div><!-- /.content-wrapper -->

                <footer class="main-footer">
                    <?php include("include/footer.php"); ?>
                </footer>

                <!-- Control Sidebar -->
                <aside class="control-sidebar control-sidebar-dark">
                    <!-- Create the tabs -->
                </aside><!-- /.control-sidebar -->
                <!-- Add the sidebar's background. This div must be placed
                     immediately after the control sidebar -->
                <div class="control-sidebar-bg"></div>

            </div><!-- ./wrapper -->

            <?php include("include/filelinks.php"); ?>

        </body>
    </html>
    <?php
} else {
    header("location:index.php");
}
?>

<script type="text/javascript">
    function selectAll() {
        if (document.getElementById("All").checked) {
            $('input[type=checkbox]').prop('checked', true);
        } else {
            $('input[type=checkbox]').prop('checked', false);
        }
    }

    function SubmitForm() {

        var r = confirm("Are You Sure You Want to Delete?");
        if (r == true) {
            document.getElementById("myForm").submit();
            //txt = "You pressed OK!";
        } else {
            //txt = "You pressed Cancel!";
        }
    }
</script>
