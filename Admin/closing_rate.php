<?php
session_start(); if(isset($_SESSION["logged_user_name"]))
{
	include_once('include/config.php');	
	
	if(isset($_POST['ajax_date'])) {
		ob_end_flush();
		$closing_date = $_POST['ajax_date'];
		//print_r($_POST['ajax_date']);
		$select_closing = "SELECT * from closing_rate where report_date='".date('Y-m-d',strtotime( str_replace('/','-',$closing_date)))."'";
		
		$rs_closing = mysqli_query($conn,$select_closing) or print(mysqli_error($conn));
		$closing_arr = array();
		if(mysqli_num_rows($rs_closing)>0){
			
			while(($data = mysqli_fetch_assoc($rs_closing))){
				$closing_arr[] = $data;
			}
		}
		echo json_encode($closing_arr);
		exit;
	}
	if(isset($_POST['item'])){
		foreach($_POST['item'] as $k=>$v) {
			if(!empty($v)) {
				$select_closing = "SELECT * from closing_rate where item_id=".$k." and report_date='".date('Y-m-d',strtotime( str_replace('/','-',$_POST['currentDate'])))."'";
				$rs_closing = mysqli_query($conn,$select_closing) or print(mysqli_error($conn));
				if(mysqli_num_rows($rs_closing)>0) {
					$sql_closing = "UPDATE closing_rate set amount= ".$v." where item_id=".$k." and report_date='".date('Y-m-d',strtotime( str_replace('/','-',$_POST['currentDate'])))."'";
					mysqli_query($conn,$sql_closing) or print(mysqli_error($conn));
				} else {
					$sql_closing = "INSERT INTO closing_rate (item_id, amount, report_date)	VALUES (".$k.", ".$v.", '".date('Y-m-d',strtotime( str_replace('/','-',$_POST['currentDate'])))."')";
					mysqli_query($conn,$sql_closing) or print(mysqli_error($conn));
				}
			}
		}
		ob_end_flush();
		header('Location: '.'closing_value_detail.php?date='. date('d-m-Y',strtotime( str_replace('/','-',$_POST['currentDate']))));
		
	}

$sSQL 	= "SELECT * FROM item_info";
$items 	= mysqli_query($conn,$sSQL) or print(mysqli_error($conn));
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
		
	</head>
    <style>
        .content{
          min-height: 0px; 
        }
    </style>
    <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Daily Soda Closing Rate
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Closing Report</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive">
				<div class="col-md-12">
					<?php include("msg.php"); ?>
                    <form action="" method="post" name="closing_rate" id="closing_rate">
						<div class="form-group has-feedback">
							<div class="row">
			                    <div class="col-md-3 col-md-offset-2">
                                    <label class="form-label">Select Report Date</label><br/>
									
									<input type="text" placeholder="DD/MM/YYYY" class="datepicker form-control" name="currentDate" value="<?php echo date('d/m/Y'); ?>"/>
								</div>
								<div class="col-md-1">
                                    <br/>
                                    <input type="button" class="btn btn-primary btn-block btn-flat" name="detail" id="detail" value="Detail">
								</div>
								<div class="col-md-2">
                                    <br/>
                                    <button type="button" class="btn btn-primary btn-block btn-flat" name="summary">Summary</button>
								</div>
								<div class="col-md-1">
                                    <br/>
                                    <button type="button" class="btn btn-primary btn-block btn-flat" name="exit" onclick="document.location.href='dashboard.php'">Exit</button>
								</div>
								<br clear="all"/>
								<br clear="all"/>
								<div class="col-md-3 col-md-offset-2">
									<label class="form-label">Rate From Excel File (Y/ N) ?</label>
								</div>
								<div class="col-md-6">
                                    <input type="text" class="form-control" name="rate_from_excel" id="rate_from_excel" value="N" style="width:65px;" />
								</div>
							</div>
							<div class="row hide">
								<div class="col-md-8 col-md-offset-2">
									 <table class="table table-bordered ">
										 <tr>
											 <td>Script</td>
											 <td>Closing Rate</td>
											 <td>Script</td>
											 <td>Closing Rate</td>
											 <td>Script</td>
											 <td>Closing Rate</td>
										 </tr>
										<?php if(mysqli_num_rows($items) > 0) { $i=0;
												while($row = mysqli_fetch_array($items)) { 
												$i++;
												 if($i == 1){ echo '<tr>'; } ?>
													 
														 <td><?php echo $row['item_name']; ?></td>
														 <td>
															<input type="text" id="<?php echo $row['id']; ?>" name="item[<?php echo $row['id']; ?>]" class="form-control">
														 </td>
												<?php if($i == 3) { 
															echo '</tr>';
															$i = 0; 
														}
													 
											 }
										} ?>
									 </table>
								</div>
								<div class="col-md-4 col-md-offset-2">
									<label class="form-label">Transfer Closing Rate to Market Rate (Y / N)?</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control" name="rate_from_excel" id="rate_from_excel" value="N" style="width:65px;" />
								</div>
								<br/>
								<br/>
								<div class="col-md-7 col-md-offset-3">
									<div class="col-md-2 col-md-offset-3">
										<input type="radio" name="print_option">&nbsp;&nbsp;Screen
									</div>
									<div class="col-md-2">
										<input type="radio" name="print_option">&nbsp;&nbsp;Printer
									</div>
									<div class="col-md-2">
										<input type="radio" name="print_option">&nbsp;&nbsp;File
									</div>
								</div>
								<br/>
								<br/>
								<br/>
								<div class="col-md-6 col-md-offset-3">
									<i>Press Ctrl + W to Exit.</i> &nbsp;&nbsp;&nbsp; OR &nbsp;&nbsp;&nbsp;<button type="submit" class="btn btn-primary btn-flat"> Submit </button>
								</div>
							</div>
						</div>
					</form>
				</div>
                
			
            </div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
?>
<script>
$(document).ready(function(){
	$( ".datepicker" ).datepicker({format: 'dd/mm/yyyy'});
	$('#detail').click(function(){
		$('.row.hide').removeClass('hide');
		$.ajax({
			url		: "closing_rate.php",
			type	: 'post',
			data	: {'ajax_date':$('.datepicker').val()},
			success	: function(result){
				result = JSON.parse(result);
				for(i=0;i<result.length;i++){
					//console.log(result[i]['id']);
					$("#"+result[i]['item_id']).val(result[i]['amount']);
				}
				//console.log(result);
			}
		});
		$(this).parents('.row').addClass('hide');
		
	});

});
</script>
