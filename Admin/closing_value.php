<?php
session_start(); if(isset($_SESSION["logged_user_name"]))
{
include_once('include/config.php');	
	

?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
    <style>
        .content{
          min-height: 0px; 
        }
    </style>
    <body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Closing Value
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Closing Value</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive">
				<div class="col-md-12">
					<?php include("msg.php"); ?>
                    <form action="" method="post" name="closing_value" id="closing_value">
						<div class="form-group has-feedback">
							<div class="row">
			                    <div class="col-md-3 col-md-offset-2">
                                    <label class="form-label">Date : </label>
									<select name="currentDate" id="currentDate" style="height:34px;width:45px;">
										<?php for($i=1;$i<=31;$i++){?>
											<?php if($i < 10){ $i = '0'.$i;} ?>
											<?php if(isset($_POST['currentDate']) && $i == $_POST['currentDate']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php
											}
											else if(!isset($_POST['currentDate']) && date('d') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
									<select name="currentMonth" id="currentMonth" style="height:34px;width:45px;">
										<?php for($i=1;$i<=12;$i++){?>
											<?php if($i < 10){ $i = '0'.$i;}?>
											<?php if(isset($_POST['currentMonth']) && $i == $_POST['currentMonth']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else if(!isset($_POST['currentMonth']) && date('m') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
									<select name="currentYear" id="currentYear" style="height:34px;width:65px;">
										<?php for($i=date('Y')-2;$i<=date('Y')+2;$i++){?>
											<?php if(isset($_POST['currentYear']) && $i == $_POST['currentYear']){?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
												<?php } else if(!isset($_POST['currentYear']) && date('Y') == $i) { ?>
												<option value="<?php echo $i;?>" selected="selected"><?php echo $i;?></option>
											<?php } else { ?>
												<option value="<?php echo $i;?>"><?php echo $i;?></option>
											<?php } ?>
										<?php }?>
									</select>
								</div>
								<!--<div class="col-md-2">
                                    <br/>
                                    <button type="submit" class="btn btn-primary btn-block btn-flat" name="summary">Summary</button>
								</div>-->
							</div>
							<div class="col-md-12">
								<div class="row">
									<br/><br/><br/><br/>
									 <div class="col-md-6">
										 <table class="table table-bordered ">
											 <tr>
												 <th>Script</th>
												 <th>&nbsp;</th>
												 <th>&nbsp;</th>
												 <th>Price</th>
											 </tr>
											 <tr>
												 <td>Gold</td>
												 <td>&nbsp;</td>
												 <td>&nbsp;</td>
												 <td>25100</td>
											 </tr>
											<tr>
												 <td>Silver</td>
												 <td>&nbsp;</td>
												 <td>&nbsp;</td>
												 <td>38000</td>
											</tr>
											<tr>
												 <td>Zinc</td>
												 <td>&nbsp;</td>
												 <td>&nbsp;</td>
												 <td>105.50</td>
											</tr>
											 
										 </table>
									 </div>
								</div>
							</div>
						</div>
					</form>
				</div>
                
			
            </div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
?>