<?php
session_start(); if(isset($_SESSION["logged_user_name"]))
{
	
	include_once('include/config.php');	
	
if(isset($_GET['mode']) && isset($_GET['id']))
{
	if($_GET['mode']==1)
	{
		$sSQL = "SELECT * FROM client_info where id=".$_GET['id'];
		$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
		if(mysqli_num_rows($rs1) > 0)
		{
			$row1 = mysqli_fetch_array($rs1);
		}
	}
	else
	{
		$sSQL = "delete from client_info where id =".$_GET['id'];
		$rs1 = mysqli_query($conn, $sSQL) or print(mysqli_error($conn));
		$_SESSION['success']="Record is deleted successfully.";
		header("Location:createClient.php");exit;
	}
}
if(isset($_POST['ok']))
{
	$client_name = $_POST['client_name'];
	$numeric_code = $_POST['numeric_code'];
	$alpha_code = $_POST['alpha_code'];
	$brok_type = $_POST['brok_type'];
	if(isset($_GET['mode']) && isset($_GET['id']) && $_GET['mode']==1)
	{
		//update query when required
    }
	else
	{
       	// insert code for all text boxes in client_info table
		$sSQL = "insert into client_info (client_name,numeric_code,alpha_code,brok_type) values ('$client_name','$numeric_code','$alpha_code','$brok_type')";
		if(mysqli_query($conn, $sSQL))
		{
			//echo "New record created successfully";
		}
		else
		{
			echo mysqli_error($conn);
			die;
		}
		$sSQL = "select id from client_info where numeric_code = '$numeric_code'";
		$rs = mysqli_query($conn,$sSQL) or print(mysqli_error($conn));
		$t = mysqli_fetch_array($rs);
		$client_id = $t['id'];
		
		$sSQL1 = "select * from item_info ORDER BY item_name";
		$rs11 = mysqli_query($conn,$sSQL1) or print(mysqli_error($conn));
		while($row11 = mysqli_fetch_array($rs11))
		{
			$item_id = $row11['id'];
			echo $row11['id'];
			$brok1 = $_POST['brok1_'.$row11['id']];
			$brok2 = $_POST['brok2_'.$row11['id']];
			if($brok1 == ""){  $brok1 = 0;  }
			if($brok2 == ""){  $brok2 = 0;  }
			$sSQL = "insert into clientbrok (client_id,item_id,brok1,brok2) values ('$client_id','$item_id','$brok1','$brok2')";
			if(mysqli_query($conn, $sSQL))
			{
				//echo "New record created successfully";
			}
			else
			{
				echo mysqli_error($conn);
				die;
			}
		}
		$_SESSION['success']="Record is inserted.";
	}
	mysqli_close($conn);
	header("Location:createClient.php");
    exit;
}
?>

<!DOCTYPE html>
<html>
	<head>
		<?php include("include/header.php"); ?>
	</head>
	<body class="skin-blue sidebar-mini">
		<div class="wrapper">
			<header class="main-header">
				<?php include("include/mainheader.php"); ?>
            </header>
			<!-- Left side column. contains the logo and sidebar -->
			<aside class="main-sidebar">
				<!-- sidebar: style can be found in sidebar.less -->
				<section class="sidebar">
				<!-- Sidebar user panel -->
					<?php include("include/leftsidebar.php"); ?>
				</section>
				<!-- /.sidebar -->
			</aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Client Information
            <!--<small>Version 2.0</small>-->
          </h1>
          <ol class="breadcrumb">
            <li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Clinet Information</li>
          </ol>
        </section>
        <!-- Main content -->
        <section class="content">
			<!-- write content here -->
			<div class="box-body table-responsive">
				<div class="col-xs-6">
					<?php include("msg.php"); ?>
					<form action="" method="post" name="client_info" id="client_info">
						  <div class="form-group has-feedback">
							<div class="row">
								<div class="col-xs-6">
									<input type="text" class="form-control" placeholder="Client Name" name="client_name"/>
								</div>
							</div>
						  </div>
						  <div class="form-group has-feedback">
							<div class="row">
								<div class="col-xs-6">
									<input type="text" class="form-control" placeholder="Numeric Code" name="numeric_code"/>
								</div>
								<div class="col-xs-6">
									<input type="text" class="form-control" placeholder="Alpha Code" name="alpha_code"/>
								</div>
							</div>
						  </div>
						  <div class="form-group has-feedback">
							<div class="row">
                                <div class="col-xs-6">
                                	<b>Brok Type : </b>
                                    <select name="brok_type" class="form-control">
                                        <option value="Fixed">Fixed</option>   
                                        <option value="Percentage">Percentage</option>   
                                    </select>
								</div>
								
							</div>
						  </div>
						  <div class="row form-group has-feedback">
						    <div class="col-xs-4">
								<label>Item</label>
							</div>
							<div class="col-xs-4">
								<label>Brok 1</label>
							</div>
							<div class="col-xs-4">
								<label>Brok 2</label>
							</div>
						  </div>
						  <div class="row form-group has-feedback">
							<?php 
								$sSQL = "SELECT * from item_info ORDER BY item_name";
								$rs = mysqli_query($conn, $sSQL) or print mysqli_error($conn);
								while($row = mysqli_fetch_array($rs)){	?>
								<div class="col-xs-4">
									<label name="item_<?php echo $row['id']; ?>"><?php echo $row['item_name']; ?></label>
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control" placeholder="Enter Brok 1" name="brok1_<?php echo $row['id']; ?>"/>
								</div>
								<div class="col-xs-4">
									<input type="text" class="form-control" placeholder="Enter Brok 2" name="brok2_<?php echo $row['id']; ?>"/>
								</div>
								
								<?php } ?>
						  </div>
						<div class="row">
							<div class="col-xs-6">
								<button type="submit" class="btn btn-primary btn-block btn-flat" name="ok">Create Client</button>
							</div>
							<div class="col-xs-6">
								<button type="reset" class="btn btn-primary btn-block btn-flat" name="reset" onClick="document.location.href='createClient.php'"/>Reset</button>
							</div>
							<!-- /.col -->
						</div>
					</form>
				</div>
				<div class="col-xs-6">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<th>Action</th>
								<th>Client Name</th>
								<th>Numeric Code</th>												
								<th>Alpha Code</th>
								<th>Brok type</th>
							</tr>
							
							<?php 
								$sSQL = "SELECT * from client_info ORDER BY client_name";
								$rs = mysqli_query($conn, $sSQL) or print mysqli_error($conn);
								while($row = mysqli_fetch_array($rs)){?>
							<tr>
								<!--<td><a href="createClient.php?id=<?php echo $row['id'];?>&mode=1">Edit</a> | <a href="createClient.php?id=<?php //echo $row['id'];?>&mode=2">Delete</a>-->
								<td><a href="javascript:delete_client(<?php echo $row['id']; ?>)">Delete</a></td>
								<td><?php echo $row['client_name'];?></td>
								<td><?php echo $row['numeric_code'];?></td>
								<td><?php echo $row['alpha_code'];?></td>
								<td><?php echo $row['brok_type'];?></td>
							</tr>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</section><!-- /.content -->
    </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <?php include("include/footer.php"); ?>
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->

	<?php include("include/filelinks.php"); ?>
	
  </body>
</html>
<?php }else
{
	header("location:index.php");
}
