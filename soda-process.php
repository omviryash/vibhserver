<script>
var items = <?php echo $json_items ?>;
var clients = <?php echo $json_clients ?>;

function soda(actiontype, lot, qty, rate, itemcode, itemname, clientcode, clientname, actiontime, actiondate, actionserial, issaved){
	this.actiontype = actiontype;
	this.lot = lot;
	this.qty = qty;
	this.rate = rate;
	this.itemcode = itemcode;
	this.itemname = itemname;
	this.clientcode = clientcode;
	this.clientname = clientname;
	this.actiontime = actiontime;
	this.actiondate = actiondate;
	this.actionserial = actionserial;
	this.issaved = issaved;
}

var sodas = [];

  
$(document).ready(function(e) {
    //$('#itemcode1').select2();
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        if (evt.keyCode == 27) {
            //alert("Escape");
            window.location="Admin/dashboard.php";
        }
    };
    
  
	$('#actiontype').focus();
	$('#selectedserial').val(0);
	
	<?php foreach($prev_data_ary as $data){ ?>
		var preventry = new soda('<?php echo $data['actiontype'] ?>',<?php echo $data['lot'] ?>,<?php echo $data['qty'] ?>,<?php echo $data['rate'] ?>,'<?php echo $data['itemcode'] ?>','<?php echo $data['itemname'] ?>','<?php echo $data['clientcode'] ?>','<?php echo $data['clientname'] ?>','<?php echo $data['actiontime'] ?>','<?php echo $data['actiondate'] ?>',<?php echo $data['actionserial'] ?>,1);
		sodas.push(preventry);
	<?php } ?>
	
	$.fn.extend({
		setSelection: function(selectionStart, selectionEnd) {
				if(this.length == 0) return this;
				input = this[0];
	
				if (input.createTextRange) {
					var range = input.createTextRange();
					range.collapse(true);
					range.moveEnd('character', selectionEnd);
					range.moveStart('character', selectionStart);
					range.select();
				} else if (input.setSelectionRange) {
					input.focus();
					input.setSelectionRange(selectionStart, selectionEnd);
				}
	
				return this;
			}
	});

	$("#lot").mask("9?999");
	$("#itemcode").mask("9?999");
	$("#actiondate").mask("99/99/9999");
	
	$('#actiondate').keypress(function(event){
		if(event.which == 13){
          	$('#sodaform').submit();
		}
	});
	
	$(document).on('focus','input',function(event){ 
		$(this).select();
	});
	
    $(document).on('keydown','.actiontype',function(event){
		if(event.which != 107 && event.which != 109 && event.which != 13){
			event.preventDefault();
		}	
	});
	
	$(document).on('keyup','.actiontype',function(event){ 
		if(event.which == 107 || event.which == 109){	
			$(this).parent().parent().find('.lot').focus();
		}
		else if(event.which == 13){
			var cval = $(this).val();
			
			if(cval == '+' || cval == '-'){
				$(this).parent().parent().find('.lot').focus();
			}
			else{
				event.preventDefault();
			}
		}
		else if(event.which == 38 || event.which == 40){
			updownkeypress(event.which);
		}
	});
	
	$(document).on('keyup','.lot',function(event){
		if(event.which == 13){
			if($.trim($(this).val()) != ''){
				$(this).parent().parent().parent().find('.itemcode').focus();
			}
			else{
				event.preventDefault();
			}
		}
		else if(event.which == 38 || event.which == 40){
			updownkeypress(event.which);
		}
	});
	
	$(document).on('keyup','.rate',function(event){
		if(event.which == 13){
			if($.trim($(this).val()) != ''){			
				$(this).parent().parent().find('.clientcode').focus();
			}
			else{
				event.preventDefault();
			}
		}
		else if(event.which == 38 || event.which == 40){
			updownkeypress(event.which);
		}
	});
	
	$(document).on('keyup','.itemcode',function(event){
      //alert($(this).val());
		if(event.which == 13 || event.which == 9){
			var item_code = $(this).val();
			var itemname = '';
			
            try{
				itemname = items[item_code].item_name;
                $('#msg').html('');	
			}
			catch(e){
				$('#msg').html('<span style="color:red">Ooops! Please enter valid item code</span>');
			}
			
			if($.trim(itemname) != ''){
				$(this).parent().parent().parent().find('.itemname').val(itemname);
				
				$(this).parent().parent().parent().find('.qty').val(items[item_code].lot_qty * $(this).parent().parent().parent().find('.lot').val());			
				$(this).parent().parent().parent().find('.rate').focus();
			}
		}
		else if(event.which == 38 || event.which == 40){
			updownkeypress(event.which);
		}
	});
	$(document).on('blur','.clientcode',function(event){
		var curctrl =  $(this);
        var myPartyCode=$(this).val();
		myPartyCode = myPartyCode.toUpperCase();
		//alert(clients[100].client_name);
        
        try{
            clientsname = clients[myPartyCode].client_name;
			$('#msg').html('');	
            if($.trim(clientsname) != ''){
				$(this).parent().parent().parent().find('.clientname').val(clientsname);
            }
        }
        catch(e){
            var tmp='';
            $(this).parent().parent().parent().find('.clientname').val('');
            $('#msg').html('<span style="color:red">Ooops! Please enter valid party code</span>');
        }
	});
	$(document).on('keyup','.clientcode',function(event){
		var curctrl =  $(this);
        if(event.which == 13){
			var currentdate = new Date(); 
			var actiontime = (currentdate.getHours()<10?('0' + currentdate.getHours()):currentdate.getHours()) + ":" + (currentdate.getMinutes()<10?('0' + currentdate.getMinutes()):currentdate.getMinutes());
			
			var location = curctrl.attr('data-location');
			
			var clientcode = curctrl.val();
			clientcode = clientcode.toUpperCase();
			var clientname = '';
			
			try{
				clientname = clients[clientcode].client_name;
				$('#msg').html('');	
			}
			catch(e){
				$('#msg').html('<span style="color:red">Ooops! Please enter valid party code</span>');
			}
			
			if($.trim(clientname) != ''){	
				curctrl.parent().parent().parent().find('.clientname').val(clientname);	
				
				var actiontype = curctrl.parent().parent().parent().find('.actiontype').val();
				var actiontype_prev = curctrl.parent().parent().parent().find('.actiontype').parent().attr('data-prev');
				
				var lot = curctrl.parent().parent().parent().find('.lot').val();
				var lot_prev = curctrl.parent().parent().parent().find('.lot').parent().attr('data-prev');
				
				var qty = curctrl.parent().parent().parent().find('.qty').val();
				var qty_prev = curctrl.parent().parent().parent().find('.qty').parent().attr('data-prev');
				
				var rate = curctrl.parent().parent().parent().find('.rate').val();
				var rate_prev = curctrl.parent().parent().parent().find('.rate').parent().attr('data-prev');
				
				var itemcode = curctrl.parent().parent().parent().find('.itemcode').val();
				var itemcode_prev = curctrl.parent().parent().parent().find('.itemcode').parent().attr('data-prev');
				
				var itemname = curctrl.parent().parent().parent().find('.itemname').val();
				
				var clientcode = curctrl.parent().parent().parent().find('.clientcode').val();
				var clientcode_prev = curctrl.parent().parent().parent().find('.clientcode').parent().attr('data-prev');
				
				var clientname = curctrl.parent().parent().parent().find('.clientname').val();
				var actiondate = $('#actiondate').val();
				
				var d = new Date();
				var day = zerocheck(d.getDate());
				var month = zerocheck(d.getMonth() + 1);
				var hour = zerocheck(d.getHours());
				var minute = zerocheck(d.getMinutes());
				var second = zerocheck(d.getSeconds());
				
				var curdatetime = day + '/' + month + '/' + d.getFullYear() + ' ' + hour + ':' + minute + ':' + second;
					
				if(location == 'tfoot'){
					var actionserial = curctrl.parent().parent().parent().find('.actionserial').val();	
					
					var newentry = new soda(actiontype,lot,qty,rate,itemcode,itemname,clientcode,clientname,actiontime,actiondate,actionserial,0);
					sodas.push(newentry);
					
					curctrl.parent().parent().parent().find('.actionserial').val(parseInt(actionserial) + 1);
					$('#selectedserial').val(parseInt(actionserial) + 1);
					
					var cls = '';
					if(actiontype == '-'){
						cls = 'minus';
					}
					
					//TOTAL
					var prev_total = $('#total').text();
					if($.trim(actiontype) == '+'){
						$('#total').text(parseInt(prev_total) + parseInt(qty * rate));
					}
					else{
						$('#total').text(prev_total - qty * rate);
					}
					
					var content = '<tr id="tr' + actionserial + '" class="' + cls + '"><td id="actiontype' + actionserial + '" data-prev="' + actiontype + '">' + actiontype + '</td><td><div id="lot' + actionserial + '" data-prev="' + lot + '" style="float:left; display:inline">' + lot + '</div><div id="qty' + actionserial + '" data-prev="' + qty + '" style="float:right; display:inline">' + qty + '</div></td><td id="rate' + actionserial + '" data-prev="' + rate + '" style="text-align:right;">' + rate + '</td><td><div id="itemname' + actionserial + '" style="float:left; display:inline" data-prev="' + itemname + '">' + itemname + '</div><div id="itemcode' + actionserial + '" style="float:right; display:inline" data-prev="' + itemcode + '">' + itemcode + '</div></td><td><div id="clientcode' + actionserial + '" data-prev="' + clientcode + '" style="float:left; display:inline">' + clientcode + '</div><div id="clientname' + actionserial + '" data-prev="' + clientname + '" style="float:right; display:inline">' + clientname + '</div></td><td id="actiontime' + actionserial + '" data-prev="' + actiontime + '">' + actiontime + '</td><td class="actionserial" id="actionserial' + actionserial + '">' + actionserial + '</td><td class="createdon" id="createdon' + actionserial + '" data-prev="' + curdatetime + '">' + curdatetime + '</td><td class="updatedon" id="updatedon' + actionserial + '"></td><td class="redtd" id="actionstatus' + actionserial + '"></td></tr>';
					
					$('#tbody').append(content);
					                    
                    /*
					curctrl.parent().parent().parent().find('.actiontype').val('');
					curctrl.parent().parent().parent().find('.lot').val('');	
					curctrl.parent().parent().parent().find('.qty').val('');		
					curctrl.parent().parent().parent().find('.rate').val('');
					curctrl.parent().parent().parent().find('.itemcode').val('');
					curctrl.parent().parent().parent().find('.itemname').val('');
					curctrl.parent().parent().parent().find('.clientcode').val('');
					curctrl.parent().parent().parent().find('.clientname').val('');
					
                    */
                   curctrl.parent().parent().parent().find('.actiontype').focus();
				   $("#rate").val('');
				   $("#lot").val('');
				}
				else if(location == 'tbody'){
					var actionserial = curctrl.parent().parent().parent().find('.actionserial').text();	
					var createdon = curctrl.parent().parent().parent().find('.createdon').text();

					var changeclass = 'redtd';
					var changestatus = curctrl.parent().parent().parent().find('.actionstatus').attr('data-prev');
					
					if(actiontype == actiontype_prev && lot == lot_prev && qty == qty_prev && rate == rate_prev && itemcode == itemcode_prev && clientcode == clientcode_prev){ 
						actiontime = curctrl.parent().parent().parent().find('.actiontime').text();	
						updatedon = curctrl.parent().parent().parent().find('.updatedon').text();
						changeclass = 'greentd';
					}
					else{
						changestatus = '0';	
						updatedon = curdatetime;				
					}
								
					var newentry = new soda(actiontype,lot,qty,rate,itemcode,itemname,clientcode,clientname,actiontime,actiondate,actionserial,changestatus);
						
					for(var i=0; i<sodas.length; i++){
						var so = sodas[i];
						if(so.actionserial == actionserial){
							sodas[i] = newentry;
							break;
						}
					}

					var cls = '';
					if(actiontype == '-'){
						cls = 'minus';
					}
					
					$('#tr' + actionserial).html('<td id="actiontype' + actionserial + '" data-prev="' + actiontype + '">' + actiontype + '</td><td><div id="lot' + actionserial + '" data-prev="' + lot + '" style="float:left; display:inline">' + lot + '</div><div id="qty' + actionserial + '" data-prev="' + qty + '" style="float:right; display:inline">' + qty + '</div></td><td id="rate' + actionserial + '" data-prev="' + rate + '" style="text-align:right;">' + rate + '</td><td><div id="itemname' + actionserial + '" data-prev="' + itemname + '" style="float:left; display:inline">' + itemname + '</div><div id="itemcode' + actionserial + '" style="float:right; display:inline" data-prev="' + itemcode + '">' + itemcode + '</div></td><td><div id="clientcode' + actionserial + '" data-prev="' + clientcode + '" style="float:left; display:inline">' + clientcode + '</div><div id="clientname' + actionserial + '" data-prev="' + clientname + '" style="float:right; display:inline">' + clientname + '</div></td><td id="actiontime' + actionserial + '" data-prev="' + actiontime + '" class="actiontime">' + actiontime + '</td><td id="actionserial' + actionserial + '" class="actionserial">' + actionserial + '</td><td class="createdon" id="createdon' + actionserial + '" data-prev="' + createdon + '">' + createdon + '</td><td class="updatedon" id="updatedon' + actionserial + '" data-prev="' + updatedon + '">' + updatedon + '</td><td data-prev="0" class="actionstatus ' + changeclass + '" id="actionstatus' + actionserial + '"></td>');
					$('#tr' + actionserial).removeClass('minus');
					$('#tr' + actionserial).addClass(cls);

					$('#selectedserial').val(parseInt(actionserial) + 1);

					var cur_actionserial = $('#actionserial').val();

					if((cur_actionserial-1) == actionserial){
						$('tfoot').find('.actiontype').focus();
						$('#selectedserial').val(0);
					}
					else{
						//EDITABLE 
						generateEditableControl(parseInt(actionserial) + 1);
						$('#tr' + actionserial).find('.actiontype').focus();
					}
				}
			}
		}
		else if(event.which == 38 || event.which == 40){
			updownkeypress(event.which);
		}
		else{
			var inputval = curctrl.val();
			var inputval_len = inputval.length;
			var icode = '';
            
            
            
//            $.each($.parseJSON('<?php echo $json_clients_j; ?>'), function(key,value){
//				if(key.substring(0,inputval_len) == inputval){
//					curctrl.val(key);
//					curctrl.setSelection(inputval_len, key.length);
//					return true;
//				}
//			});
		}
	});
});

function zerocheck(i){
	if(i<10){
		return '0' + i;
	}
	else{
		return i;
	}
}

function updownkeypress(keyvalue){
	if(keyvalue == 38){
		var selserial = $('#selectedserial').val();
		
		if($.trim(selserial) == '' || selserial == 0){
			var selserial = $('#actionserial').val();
		}

		if(selserial - 1 >= 1){
			$('#selectedserial').val(selserial - 1);

			//EDITABLE 
			generateEditableControl(selserial - 1);
			
			//READONLY
			generateReadonlyControl(selserial);
		}
	}
	else if(keyvalue == 40){
		var selserial = $('#selectedserial').val();
		var actserial = $('#actionserial').val();

		if(selserial > 0 && selserial < (actserial - 1)){
			$('#selectedserial').val(parseInt(selserial) + 1);

			//EDITABLE 
			generateEditableControl(parseInt(selserial) + 1);
		}
		else{
			$('#actiontype').focus();
			$('#selectedserial').val(0);
		}
		
		//READONLY
		generateReadonlyControl(selserial);
	}
}

function generateReadonlyControl(serialno){
	var controlary = ['actiontype','lot','qty','rate','itemname','itemcode','clientname','clientcode'];
	
	for(var i=0; i<controlary.length; i++){
		var controlname = controlary[i];
		var ctrl_cur_val = $('#new' + controlname + serialno).val(); 
		var ctrl_prev_val = $('#new' + controlname + serialno).parent().attr('data-prev'); 
		
		if($.trim(ctrl_cur_val) != ''){
			$('#' + controlname + serialno).text(ctrl_cur_val);
			$('#' + controlname + serialno).attr('data-prev',ctrl_cur_val);
		}
		else{
			$('#' + controlname + serialno).text(ctrl_prev_val);
		}
	}
}

function generateEditableControl(curserial){ 
	var controlary = ['actiontype','lot','qty','rate','itemname','itemcode','clientname','clientcode'];
	
	for(var i=0; i<controlary.length; i++){
		var controlname = controlary[i];
		var readonly = '';
		var textalign = 'left';
		
		if(controlname == 'qty' || controlname == 'itemname' || controlname == 'clientname' || controlname == 'actionserial' ){
			readonly = 'readonly="readonly"';
		}
		
		if(controlname == 'qty' || controlname == 'rate' || controlname == 'itemcode' || controlname == 'clientname' ){
			textalign = 'right';
		}
				
		var ctrl_cur_val = $('#' + controlname + curserial).text();
		
		if(controlname == 'actiontype'){
			$('#' + controlname + curserial).html('<input class="edit ' + controlname + '" type="text" maxlength="1" id="new' + controlname + curserial + '" style="width:30px" ' + readonly + ' value="' + ctrl_cur_val + '">');
		}
		else if(controlname == 'clientcode'){
			$('#' + controlname + curserial).html('<input class="edit ' + controlname + '" data-location="tbody" type="text" id="new' + controlname + curserial + '" style="width:60px" ' + readonly + ' value="' + ctrl_cur_val + '">');
		}
		else{
			$('#' + controlname + curserial).html('<input class="edit ' + controlname + '" type="text" id="new' + controlname + curserial + '" style="width:60px;text-align:' + textalign + ';" ' + readonly + ' value="' + ctrl_cur_val + '">');
		}
	}
	
	$('#newactiontype' + curserial).focus(); 
}

function savedata(){
	var unsaved_sodas = [];

	for(var i=0; i<sodas.length; i++){
		if(sodas[i].issaved == 0){
			unsaved_sodas.push(sodas[i]);
		}
	}
    
    //alert(sodas.length);
 
    
    
    //$.post('aj-savedata.php', {ary:JSON.stringify(unsaved_sodas)}, function(data){/*stuff*/ $( "#result" ).html(data); }, 'json');
    
    $.ajax({
        type:  'post', 
       cache:  false ,
        url:  'aj-savedata.php',
        data:  {result :JSON.stringify(unsaved_sodas)},
        success: function(resp) {
          var myJSONObject = {"result": JSON.parse(resp)};
          if(myJSONObject.result.length >= 1){
            for(var i=0;i < myJSONObject.result.length; i++){
              var key=myJSONObject.result[i];
              $('#actionstatus' + key).removeClass('redtd');
              $('#actionstatus' + key).addClass('greentd');
              $('#actionstatus' + key).attr('data-prev','1');
              sodas[key].issaved = 1;
              //$( "#result" ).html(key);
            }
            //alert("123");
          }
          
          //$( "#result" ).html(myJSONObject.result.length);
          $( "#result2" ).html(JSON.parse(resp));
          
          //$( "#result" ).html(resp);
        } 

      });                   
   
}

setInterval(function(){ savedata(); }, 2000);
</script>