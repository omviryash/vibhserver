--
-- Database: `vibhserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE `clientbrok` (
  `client_brok_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `brok1` double NOT NULL,
  `brok2` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientbrok`
--

INSERT INTO `clientbrok` (`client_brok_id`, `client_id`, `item_id`, `brok1`, `brok2`) VALUES
(1, 1, 2, 0.001, 0),
(2, 1, 6, 0.001, 0),
(3, 1, 10, 0.001, 0),
(4, 1, 9, 0.001, 0),
(5, 1, 5, 0.001, 0),
(6, 1, 3, 0.001, 0),
(7, 1, 7, 0.001, 0),
(8, 1, 8, 0.001, 0),
(9, 1, 4, 0.001, 0),
(10, 2, 2, 100, 200),
(11, 2, 6, 100, 200),
(12, 2, 10, 100, 200),
(13, 2, 9, 100, 200),
(14, 2, 5, 100, 200),
(15, 2, 3, 100, 200),
(16, 2, 7, 100, 200),
(17, 2, 8, 100, 200),
(18, 2, 4, 100, 200),
(19, 3, 2, 250, 0),
(20, 3, 6, 250, 0),
(21, 3, 10, 250, 0),
(22, 3, 9, 250, 0),
(23, 3, 5, 250, 0),
(24, 3, 3, 250, 0),
(25, 3, 7, 250, 0),
(26, 3, 8, 250, 0),
(27, 3, 4, 250, 0),
(28, 4, 2, 0.001, 0.002),
(29, 4, 6, 0.001, 0.002),
(30, 4, 10, 0.001, 0.002),
(31, 4, 9, 0.001, 0.002),
(32, 4, 5, 0.001, 0.002),
(33, 4, 3, 0.001, 0.002),
(34, 4, 7, 0.001, 0.002),
(35, 4, 8, 0.001, 0.002),
(36, 4, 4, 0.001, 0.002);

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE `client_info` (
  `id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `numeric_code` varchar(10) DEFAULT NULL,
  `alpha_code` varchar(50) NOT NULL,
  `brok_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`id`, `client_name`, `numeric_code`, `alpha_code`, `brok_type`) VALUES
(1, 'Client 1', '001', 'Client_1', 'Percentage'),
(2, 'Client 2', '002', 'Client_2', 'Fixed'),
(3, 'Client 3', '003', 'Client_3', 'Fixed'),
(4, 'Client 4', '004', 'Client_4', 'Percentage');

-- --------------------------------------------------------

--
-- Table structure for table `item_info`
--

CREATE TABLE `item_info` (
  `id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `lot_qty` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_info`
--

INSERT INTO `item_info` (`id`, `item_code`, `item_name`, `lot_qty`) VALUES
(10, '50', 'GOLD', 10),
(2, '30', 'COPPER', 100),
(3, '7', 'NICKEL', 25),
(4, '8', 'ZINC', 500),
(5, '6', 'LEAD', 500),
(6, '4', 'CRUDEOIL', 10),
(7, '15', 'SILVER', 3),
(8, '12', 'SILVERMINI', 1),
(9, '24', 'GOLDMINI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `soda_info`
--

CREATE TABLE `soda_info` (
  `id` int(11) NOT NULL,
  `actiondate` varchar(20) NOT NULL,
  `entry_type` varchar(1) NOT NULL,
  `lot` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `rate` decimal(18,2) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_code` varchar(20) NOT NULL,
  `actiontime` varchar(20) NOT NULL,
  `serial_no` int(11) NOT NULL,
  `createdon` varchar(20) NOT NULL,
  `updatedon` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soda_info`
--

INSERT INTO `soda_info` (`id`, `actiondate`, `entry_type`, `lot`, `qty`, `rate`, `item_name`, `item_code`, `client_name`, `client_code`, `actiontime`, `serial_no`, `createdon`, `updatedon`) VALUES
(1, '23/01/2016', '-', 1, 100, '12500.00', 'COPPER', '30', 'Client 2', '002', '13:35', 1, '1453525836', '1453622863'),
(2, '23/01/2016', '+', 5, 250000, '12500.00', 'LEAD', '6', 'Client 2', '002', '10:41', 2, '1453525896', ''),
(3, '23/01/2016', '-', 5, 250000, '12500.00', 'LEAD', '30', 'Client 2', '002', '10:41', 3, '1453525920', '1453527864'),
(4, '23/01/2016', '-', 1, 250000, '100.00', 'LEAD', '30', 'Client 4', '004', '10:42', 4, '1453525974', ''),
(5, '23/01/2016', '-', 2, 100, '25000.00', 'SILVERMINI', '12', 'Client 1', '001', '11:18', 5, '1453528102', '1453528140'),
(6, '23/01/2016', '+', 200, 200, '25000.00', 'SILVERMINI', '12', 'Client 1', '001', '13:38', 6, '1453622936', '1453623070'),
(7, '23/01/2016', '-', 2, 200, '25000.00', 'SILVERMINI', '12', 'Client 1', '001', '13:39', 7, '1453622954', ''),
(8, '24/01/2016', '+', 1, 100, '12500.00', 'COPPER', '30', 'Client 1', '001', '14:53', 1, '1453627404', '1453643789'),
(9, '24/01/2016', '-', 2, 200, '12500.00', 'COPPER', '30', 'Client 1', '001', '14:53', 2, '1453627420', ''),
(10, '24/01/2016', '-', 2, 200, '12501.00', 'COPPER', '30', 'Client 1', '001', '14:54', 3, '1453627442', '1453643789'),
(11, '24/01/2016', '-', 2, 1000, '11000.00', 'LEAD', '6', 'Client 1', '001', '14:54', 4, '1453627462', ''),
(12, '24/01/2016', '+', 4, 2000, '11501.00', 'LEAD', '6', 'Client 1', '001', '14:54', 5, '1453627490', '1453643789'),
(13, '24/01/2016', '+', 3, 3, '24250.00', 'SILVERMINI', '12', 'Client 1', '001', '14:55', 6, '1453627534', ''),
(14, '24/01/2016', '-', 6, 6, '24700.00', 'SILVERMINI', '12', 'Client 1', '001', '14:55', 7, '1453627550', '1453643789'),
(15, '24/01/2016', '+', 6, 6, '24750.00', 'SILVERMINI', '12', 'Client 1', '001', '14:56', 8, '1453627576', ''),
(16, '24/01/2016', '+', 1, 100, '12500.00', 'COPPER', '30', 'Client 2', '002', '19:27', 9, '1453643844', '1453644132'),
(17, '24/01/2016', '-', 2, 200, '12500.00', 'COPPER', '30', 'Client 2', '002', '19:27', 10, '1453643866', ''),
(18, '24/01/2016', '-', 2, 200, '12501.00', 'COPPER', '30', 'Client 2', '002', '19:28', 11, '1453643882', '1453644132'),
(19, '24/01/2016', '+', 4, 2000, '11501.00', 'LEAD', '6', 'Client 2', '002', '19:28', 12, '1453643934', ''),
(20, '24/01/2016', '-', 2, 1000, '11000.00', 'LEAD', '6', 'Client 2', '002', '19:29', 13, '1453643962', '1453644132'),
(22, '24/01/2016', '-', 6, 6, '24700.00', 'SILVERMINI', '12', 'Client 2', '002', '19:30', 15, '1453644058', '1453644132'),
(23, '24/01/2016', '+', 3, 3, '24250.00', 'SILVERMINI', '12', 'Client 2', '002', '19:31', 16, '1453644080', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `full_name`, `username`, `pass`) VALUES
(1, 'Admin', 'admin', 'admin');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientbrok`
--
ALTER TABLE `clientbrok`
  ADD PRIMARY KEY (`client_brok_id`);

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_code` (`numeric_code`);

--
-- Indexes for table `item_info`
--
ALTER TABLE `item_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_code` (`item_code`);

--
-- Indexes for table `soda_info`
--
ALTER TABLE `soda_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientbrok`
--
ALTER TABLE `clientbrok`
  MODIFY `client_brok_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `client_info`
--
ALTER TABLE `client_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `item_info`
--
ALTER TABLE `item_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `soda_info`
--
ALTER TABLE `soda_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
