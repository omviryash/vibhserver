-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2016 at 03:15 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vibhserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `menu_info`
--

CREATE TABLE `menu_info` (
  `id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `access_user_type` varchar(255) DEFAULT NULL COMMENT 'Add the usertype seperated by "," to whom you need to show these links',
  `parent_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_info`
--

INSERT INTO `menu_info` (`id`, `title`, `link`, `access_user_type`, `parent_id`) VALUES
(1, 'Master', '#', '1,2', 0),
(2, 'Party', 'createClient.php', '1,2', 1),
(3, 'Script', 'createItem.php', '1,2', 1),
(4, 'Transaction', '#', '1,2', 0),
(5, 'Soda Entry', '../index.php', '1,2', 4),
(6, 'Closing Rate', './closing_rate.php', '1,2', 4),
(7, 'Forward Soda', './forward_soda.php', '1,2', 4),
(8, 'Reports', '#', '1,2', 0),
(9, 'Rate Variation', '#', '1,2', 8),
(10, 'Outstanding', '#', '1,2', 8),
(11, 'Daily Soda Posi.', 'soda_list.php', '1,2', 8),
(12, 'Text File', '#', '1,2', 8),
(13, 'Closing Val. File', './closing_value.php', '1,2', 8),
(14, 'Monthly Forward', '#', '1,2', 8),
(15, 'Brokerage Report', './soda_list_brok.php', '1,2', 8),
(16, 'Utility', '#', '1,2', 0),
(17, 'Backup', '#', '1,2', 16),
(18, 'Restore', '#', '1,2', 16),
(19, 'Closing Val. Detail', './closing_value_detail.php', '1,2', 8);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `menu_info`
--
ALTER TABLE `menu_info`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `menu_info`
--
ALTER TABLE `menu_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
