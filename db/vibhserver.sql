-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2016 at 09:21 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `vibhserver`
--

-- --------------------------------------------------------

--
-- Table structure for table `clientbrok`
--

CREATE TABLE `clientbrok` (
  `client_brok_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `brok1` double NOT NULL,
  `brok2` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `clientbrok`
--

INSERT INTO `clientbrok` (`client_brok_id`, `client_id`, `item_id`, `brok1`, `brok2`) VALUES
(1, 1, 2, 0, 0),
(2, 1, 6, 0, 0),
(3, 1, 10, 0, 0),
(4, 1, 9, 0, 0),
(5, 1, 5, 0, 0),
(6, 1, 3, 0, 0),
(7, 1, 7, 0, 0),
(8, 1, 8, 0, 0),
(9, 1, 4, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE `client_info` (
  `id` int(11) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `numeric_code` varchar(10) DEFAULT NULL,
  `alpha_code` varchar(50) NOT NULL,
  `brok_type` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`id`, `client_name`, `numeric_code`, `alpha_code`, `brok_type`) VALUES
(1, 'First', '001', 'a1', 'Fixed');

-- --------------------------------------------------------

--
-- Table structure for table `item_info`
--

CREATE TABLE `item_info` (
  `id` int(11) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `lot_qty` double NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_info`
--

INSERT INTO `item_info` (`id`, `item_code`, `item_name`, `lot_qty`) VALUES
(10, '50', 'GOLD', 10),
(2, '30', 'COPPER', 100),
(3, '7', 'NICKEL', 25),
(4, '8', 'ZINC', 500),
(5, '6', 'LEAD', 500),
(6, '4', 'CRUDEOIL', 10),
(7, '15', 'SILVER', 3),
(8, '12', 'SILVERMINI', 0.5),
(9, '24', 'GOLDMINI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_info`
--

CREATE TABLE `menu_info` (
  `id` int(11) NOT NULL,
  `title` varchar(500) DEFAULT NULL,
  `link` varchar(500) DEFAULT NULL,
  `access_user_type` varchar(255) DEFAULT NULL COMMENT 'Add the usertype seperated by "," to whom you need to show these links',
  `parent_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_info`
--

INSERT INTO `menu_info` (`id`, `title`, `link`, `access_user_type`, `parent_id`) VALUES
(1, 'Master', '#', '1,2', 0),
(2, 'Party', 'createClient.php', '1,2', 1),
(3, 'Script', 'createItem.php', '1,2', 1),
(4, 'Transaction', '#', '1,2', 0),
(5, 'Soda Entry', '../index.php', '1,2', 4),
(6, 'Closing Rate', './closing_rate.php', '1,2', 4),
(7, 'Forward Soda', './forward_soda.php', '1,2', 4),
(8, 'Reports', '#', '1,2', 0),
(9, 'Rate Variation', '#', '1,2', 8),
(10, 'Outstanding', '#', '1,2', 8),
(11, 'Daily Soda Posi.', 'soda_list.php', '1,2', 8),
(12, 'Text File', '#', '1,2', 8),
(13, 'Closing Val. File', './closing_value.php', '1,2', 8),
(14, 'Monthly Forward', '#', '1,2', 8),
(15, 'Brokerage Report', './soda_list_brok.php', '1,2', 8),
(16, 'Utility', '#', '1,2', 0),
(17, 'Backup', '#', '1,2', 16),
(18, 'Restore', '#', '1,2', 16),
(19, 'Closing Val. Detail', './closing_value_detail.php', '1,2', 8);

-- --------------------------------------------------------

--
-- Table structure for table `soda_info`
--

CREATE TABLE `soda_info` (
  `id` int(11) NOT NULL,
  `actiondate` varchar(20) NOT NULL,
  `entry_type` varchar(1) NOT NULL,
  `lot` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `rate` decimal(18,2) NOT NULL,
  `item_name` varchar(255) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `client_name` varchar(255) NOT NULL,
  `client_code` varchar(20) NOT NULL,
  `actiontime` varchar(20) NOT NULL,
  `serial_no` int(11) NOT NULL,
  `createdon` varchar(20) NOT NULL,
  `updatedon` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `soda_info`
--

INSERT INTO `soda_info` (`id`, `actiondate`, `entry_type`, `lot`, `qty`, `rate`, `item_name`, `item_code`, `client_name`, `client_code`, `actiontime`, `serial_no`, `createdon`, `updatedon`) VALUES
(1, '17/03/2016', '+', 2, 20, '25000.00', 'GOLD', '50', 'First', '001', '14:08', 1, '1458203902', '1458204934'),
(2, '17/03/2016', '+', 2, 20, '25000.00', 'GOLD', '50', 'First', '001', '14:08', 2, '1458203906', ''),
(3, '17/03/2016', '+', 2, 20, '25000.00', 'GOLD', '50', 'First', '001', '14:08', 3, '1458203912', '1458204934');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `userid` int(11) NOT NULL,
  `full_name` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `pass` varchar(50) NOT NULL,
  `usertype` int(11) NOT NULL COMMENT '1-Super Admin, 2-trade Admin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`userid`, `full_name`, `username`, `pass`, `usertype`) VALUES
(1, 'Admin', 'admin', 'admin', 1),
(2, 'kajal', 'kajal', '123456', 2);


-- Dumping structure for table vibhserver.closing_rate
CREATE TABLE IF NOT EXISTS `closing_rate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` bigint(20) DEFAULT NULL,
  `amount` float DEFAULT NULL,
  `report_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `clientbrok`
--
ALTER TABLE `clientbrok`
  ADD PRIMARY KEY (`client_brok_id`);

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `client_code` (`numeric_code`);

--
-- Indexes for table `item_info`
--
ALTER TABLE `item_info`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_code` (`item_code`);

--
-- Indexes for table `menu_info`
--
ALTER TABLE `menu_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `soda_info`
--
ALTER TABLE `soda_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `clientbrok`
--
ALTER TABLE `clientbrok`
  MODIFY `client_brok_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `client_info`
--
ALTER TABLE `client_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `item_info`
--
ALTER TABLE `item_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `menu_info`
--
ALTER TABLE `menu_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `soda_info`
--
ALTER TABLE `soda_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
