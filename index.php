<?php 
include "includes/config.php";
ob_start();
if(!isset($_SESSION)) 
    { 
        session_start(); 
    } 
if(isset($_SESSION["logged_user_name"]))
{
//CLIENT INFO
$clients = $db->get_results("SELECT client_name,UPPER(numeric_code) as numeric_code,UPPER(alpha_code) as alpha_code FROM `client_info` ORDER BY `numeric_code` ASC");
$client_ary = array();

//echo "<pre>";print_r($clients); die;
if($clients) {
    foreach($clients as $c){
        $client_ary[$c->numeric_code] = $c;
        $client_ary[$c->alpha_code] = $c;
    }    
}
$json_clients = json_encode($client_ary); 

$clients_j = $db->get_results("SELECT * FROM `client_info` ORDER BY numeric_code ASC",ARRAY_A);
$client_ary_j = array();
if($clients_j) {
    foreach($clients_j as $c){
        $client_ary_j[$c['numeric_code']] = $c;
    }    
}

$json_clients_j = json_encode($client_ary_j);


//ITEM INFO
$items = $db->get_results("select * from item_info order by item_code asc");
$item_ary = array();

foreach($items as $i){
	$item_ary[$i->item_code] = $i;
}

$json_items = json_encode($item_ary);

//ACTION DATE
if(isset($_POST['actiondate'])){
	$actiondate =  $_POST['actiondate'];
}
else{
	$actiondate = date('d/m/Y');
}

$prev_data_ary = array();
$prev_data = mysql_query("select * from soda_info where actiondate='".$actiondate."' order by serial_no asc");
while($pdata = mysql_fetch_array($prev_data)){
	$prev_row_ary = array();
	$prev_row_ary['actiontype'] = $pdata['entry_type'];
	$prev_row_ary['lot'] = $pdata['lot'];
	$prev_row_ary['qty'] = $pdata['qty'];
	$prev_row_ary['rate'] = $pdata['rate'];
	$prev_row_ary['itemcode'] = $pdata['item_code'];
	$prev_row_ary['itemname'] = $pdata['item_name'];
	$prev_row_ary['clientcode'] = $pdata['client_code'];
	$prev_row_ary['clientname'] = $pdata['client_name'];
	$prev_row_ary['actiontime'] = $pdata['actiontime'];
	$prev_row_ary['actiondate'] = $pdata['actiondate'];
	$prev_row_ary['createdon'] = $pdata['createdon'];
	$prev_row_ary['updatedon'] = $pdata['updatedon'];
	//$prev_row_ary['updatedon'] = NULL;
	$prev_row_ary['actionserial'] = $pdata['serial_no'];
	$prev_row_ary['issaved'] = '1';
	
	$prev_data_ary[] = $prev_row_ary;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>&#2384; | Daily Soda Entry</title>
<link href="css/style.css" type="text/css" rel="stylesheet" />
<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/jquery.maskedinput.min.js" type="text/javascript"></script>
<!--<script src="js/soda.js" type="text/javascript"></script>-->
<script>
var today=new Date(<?php echo date('Y').','.date('m').','.date('d').','.date('H').','.date('i').','.date('s').',0' ?>);

/*function startTime() {
   	today.setSeconds(today.getSeconds() + 1);
    var h=today.getHours() > 12 ? today.getHours() - 12 : today.getHours();
    var m=today.getMinutes();
    var s=today.getSeconds();

	h = checkTime(h);
    m = checkTime(m);
    s = checkTime(s);

    document.getElementById('curtime').innerHTML = h+":"+m+":"+s;
    var t = setTimeout(function(){startTime()},1000);
}
function checkTime(i) {
    if (i<10) {i = "0" + i};  // add zero in front of numbers < 10
    return i;
}*/

function startTime() {
        var date = new Date();
        var hours = date.getHours() > 12 ? date.getHours() - 12 : date.getHours();
        var am_pm = date.getHours() >= 12 ? "PM" : "AM";
        hours = hours < 10 ? "0" + hours : hours;
        var minutes = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        var seconds = date.getSeconds() < 10 ? "0" + date.getSeconds() : date.getSeconds();
        time = hours + ":" + minutes + ":" + seconds + " " + am_pm;
        var lblTime = document.getElementById("curtime");
		var t = setTimeout(function(){startTime()},1000);
        curtime.innerHTML = time;
    };


</script>


  <?php include "soda-process.php" ?>

</head>

<body onload="startTime()">
<a href="./Admin/dashboard.php">Menu</a>
<form id="sodaform" method="post">
  <table border="0" cellpadding="5" cellspacing="5">
    <tr>
      <td style="width:400px"> Date:
        <input type="text" id="actiondate" name="actiondate" value="<?php echo $actiondate ?>" />
        <span id="curtime"></span></td>
      <td id="msg"></td>
    </tr>
    <tr>
      <td colspan="2"><table width="100%" border="1" cellpadding="2" cellspacing="" class="tbl">
          
            <th>+/-</th>
            <th> <div style="float:left; display:inline">Lot</div>
              <div style="float:right; display:inline">Qty</div></th>
            <th style="text-align:right">Rate</th>
            <th>Script</th>
            <th>Party</th>
            <th>Time</th>
            <th>Serial</th>
            <th>Crearted</th>
            <th>Updated</th>
            <th style="width:10px"></th>
          </tr>
            </thead>
          
          <tbody id="tbody">
            <?php 
	//$prev_data = mysql_query("select * from soda_info where actiondate='".$actiondate."' order by serial_no asc");
	$actionserial = "0";
	$total = 0;
	
	//if(mysql_num_rows($prev_data)){
	//	while($row = mysql_fetch_array($prev_data)){
	foreach($prev_data_ary as $row){
		$total += $row["rate"] * $row["qty"];
	?>
            <tr id="tr<?php echo $row["actionserial"] ?>" class="<?php echo ($row["actiontype"]=='-'?'minus':'')?>">
              <td data-prev="<?php echo $row["actiontype"];?>" id="actiontype<?php echo $row["actionserial"] ?>"><?php echo $row["actiontype"];?></td>
              <td><div data-prev="<?php echo $row["lot"];?>" id="lot<?php echo $row["actionserial"] ?>" style="float:left; display:inline"><?php echo $row["lot"];?></div>
                <div data-prev="<?php echo $row["qty"];?>" id="qty<?php echo $row["actionserial"] ?>" style="float:right; display:inline"><?php echo $row["qty"];?></div></td>
              <td align="right" data-prev="<?php echo $row["rate"];?>" id="rate<?php echo $row["actionserial"] ?>"><?php echo $row["rate"];?></td>
              <td><div id="itemname<?php echo $row["actionserial"] ?>" style="float:left; display:inline"><?php echo $row["itemname"];?></div>
                <div data-prev="<?php echo $row["itemcode"];?>" id="itemcode<?php echo $row["actionserial"] ?>" style="float:right; display:inline"><?php echo $row["itemcode"];?></div></td>
              <td><div data-prev="<?php echo $row["clientcode"];?>" id="clientcode<?php echo $row["actionserial"] ?>" style="float:left; display:inline"><?php echo $row["clientcode"];?></div>
                <div id="clientname<?php echo $row["actionserial"] ?>" style="float:right; display:inline"><?php echo $row["clientname"];?></div></td>
              <td class="actiontime" id="actiontime<?php echo $row["actionserial"] ?>"><?php echo $row["actiontime"];?></td>
              <td class="actionserial" id="actionserial<?php echo $row["actionserial"] ?>"><?php echo $row["actionserial"];?></td>
              <td class="createdon"><?php echo date('d/m/Y H:i:s',$row["createdon"]) ?></td>
              <td class="updatedon"><?php echo $row["updatedon"]!=""?date('d/m/Y H:i:s',$row["updatedon"]):"" ?></td>
              <td data-prev="1" class="greentd actionstatus"></td>
            </tr>
            <?php $actionserial = $row["actionserial"];?>
            <?php }
	//}
?>
          </tbody>
          <tfoot>
            <tr>
              <td></td>
              <td colspan="2"><strong style="float:left">Total</strong> <strong id="total" style="float:right"><?php echo $total ?></strong></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
            <tr>
              <td><input id="actiontype" class="actiontype" style="width:30px; text-align:center;" maxlength="1" placeholder="+/-" /></td>
              <td><div style="float:left; display:inline">
                  <input id="lot" class="lot" style="width:60px" placeholder="Lot" />
                </div>
                <div style="float:right; display:inline">
                  <input id="qty" class="qty" style="width:60px;text-align:right;" readonly="readonly" placeholder="Qty" />
                </div></td>
              <td style="text-align:right;"><input id="rate" class="rate" style="width:100px;text-align:right;" placeholder="Rate"  /></td>
              <td><div style="float:left; display:inline">
                  <input id="itemname" class="itemname" style="width:100px" readonly="readonly" placeholder="Script Name" />
                </div>
                <div style="float:right; display:inline">
                  <input id="itemcode" class="itemcode" style="width:70px; margin-left:10px; text-align:right;" placeholder="Script Code" />
                </div></td>
              <td><div style="float:left; display:inline">
                  <input id="clientcode" class="clientcode" data-location="tfoot" style="width:60px" placeholder="Party Code" />
                </div>
                <div style="float:right; display:inline">
                  <input id="clientname" class="clientname" style="width:100px; margin-left:10px; text-align:right;" readonly="readonly" placeholder="Party Name" />
                </div></td>
              <td></td>
              <td><input type="hidden" id="actionserial" class="actionserial" value="<?php echo $actionserial+1;?>" />
                <input type="hidden" id="selectedserial" value="0" /></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tfoot>
        </table></td>
    </tr>
  </table>
</form>
</body>
</html>
<?php }else
{
	header("location:Admin/index.php");
}

